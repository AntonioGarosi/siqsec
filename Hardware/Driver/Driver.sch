<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="38" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="51" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="00-DDD-CONNECTORS" urn="urn:adsk.eagle:library:14328941">
<packages>
<package name="MOLEX-KK254-4X-TOP" urn="urn:adsk.eagle:footprint:14328975/2" library_version="14">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<wire x1="-5.1" y1="-2.9" x2="-5.1" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.1" y1="1.27" x2="-5.1" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.1" y1="1.27" x2="5.1" y2="-2.9" width="0.1524" layer="21"/>
<wire x1="-5.1" y1="-2.9" x2="5.1" y2="-2.9" width="0.1524" layer="21"/>
<wire x1="-5.1" y1="1.27" x2="-5.1" y2="2.9" width="0.1524" layer="21"/>
<wire x1="5.1" y1="2.9" x2="-5.1" y2="2.9" width="0.1524" layer="21"/>
<wire x1="5.1" y1="2.9" x2="5.1" y2="1.27" width="0.1524" layer="21"/>
<pad name="4" x="-3.81" y="0" drill="0.9" diameter="1.4" shape="long" rot="R270"/>
<pad name="3" x="-1.27" y="0" drill="0.9" diameter="1.4" shape="long" rot="R270"/>
<pad name="2" x="1.27" y="0" drill="0.9" diameter="1.4" shape="long" rot="R270"/>
<pad name="1" x="3.81" y="0" drill="0.9" diameter="1.4" shape="long" rot="R270"/>
<text x="-5.08" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="27">4X1</text>
</package>
<package name="MOLEX-KK254-4X-SIDE" urn="urn:adsk.eagle:footprint:14328978/1" library_version="14">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<pad name="4" x="-3.81" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R270"/>
<pad name="3" x="-1.27" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R270"/>
<pad name="2" x="1.27" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R270"/>
<pad name="1" x="3.81" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R270"/>
<text x="-5.08" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-5.08" size="1.27" layer="27">4X1</text>
<wire x1="-5.09" y1="-5.9" x2="-5.09" y2="-2.72" width="0.1524" layer="21"/>
<wire x1="5.09" y1="-2.72" x2="-5.09" y2="-2.72" width="0.1524" layer="21"/>
<wire x1="-5.09" y1="-5.9" x2="-3.81" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-5.9" x2="3.81" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-5.9" x2="5.09" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="5.09" y1="-2.72" x2="5.09" y2="-5.9" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="-2.54" width="0.6096" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="3.81" y2="-2.54" width="0.6096" layer="21"/>
<wire x1="-3.81" y1="-5.9" x2="-3.81" y2="-15.69" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-15.69" x2="3.81" y2="-15.69" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-15.69" x2="3.81" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-2.54" width="0.6096" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-2.54" width="0.6096" layer="21"/>
</package>
<package name="MOLEX-KK254-2X-TOP" urn="urn:adsk.eagle:footprint:14328957/1" library_version="18">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<wire x1="-2.55" y1="-2.9" x2="-2.55" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.55" y1="1.27" x2="-2.55" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.55" y1="-2.9" x2="2.55" y2="-2.9" width="0.1524" layer="21"/>
<wire x1="-2.55" y1="1.27" x2="-2.55" y2="2.9" width="0.1524" layer="21"/>
<wire x1="2.55" y1="2.9" x2="-2.55" y2="2.9" width="0.1524" layer="21"/>
<wire x1="2.55" y1="1.27" x2="2.55" y2="-2.9" width="0.1524" layer="21"/>
<wire x1="2.55" y1="2.9" x2="2.55" y2="1.27" width="0.1524" layer="21"/>
<pad name="2" x="-1.27" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="1" x="1.27" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R90"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.27" layer="27">2X1</text>
</package>
<package name="MOLEX-KK254-2X-SIDE" urn="urn:adsk.eagle:footprint:14328974/1" library_version="18">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<wire x1="-2.55" y1="-5.9" x2="-2.55" y2="-2.72" width="0.1524" layer="21"/>
<wire x1="2.55" y1="-2.72" x2="-2.55" y2="-2.72" width="0.1524" layer="21"/>
<wire x1="-2.55" y1="-5.9" x2="-1.27" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-5.9" x2="1.27" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-5.9" x2="2.55" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="2.55" y1="-2.72" x2="2.55" y2="-5.9" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-2.54" width="0.6096" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-2.54" width="0.6096" layer="21"/>
<wire x1="-1.27" y1="-5.9" x2="-1.27" y2="-14.42" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-14.42" x2="1.27" y2="-14.42" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-14.42" x2="1.27" y2="-5.9" width="0.1524" layer="51"/>
<pad name="2" x="-1.27" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="1" x="1.27" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R90"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-5.08" size="1.27" layer="27">2X1</text>
</package>
<package name="MOLEX-KK254-3X-TOP" urn="urn:adsk.eagle:footprint:14328970/2" library_version="18">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<wire x1="-3.81" y1="-1.68" x2="-3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="-3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.68" x2="3.81" y2="-1.68" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="4.22" width="0.1524" layer="21"/>
<wire x1="3.81" y1="4.22" x2="-3.81" y2="4.22" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-1.68" width="0.1524" layer="21"/>
<wire x1="3.81" y1="4.22" x2="3.81" y2="2.54" width="0.1524" layer="21"/>
<pad name="3" x="-2.54" y="1.27" drill="0.9" diameter="1.4" shape="long" rot="R90"/>
<pad name="2" x="0" y="1.27" drill="0.9" diameter="1.4" shape="long" rot="R90"/>
<pad name="1" x="2.54" y="1.27" drill="0.9" diameter="1.4" shape="long" rot="R90"/>
<text x="-3.81" y="4.58" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.27" layer="27">3X1</text>
</package>
<package name="MOLEX-KK254-3X-SIDE" urn="urn:adsk.eagle:footprint:14328968/1" library_version="18">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<pad name="3" x="-2.54" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="2" x="0" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="1" x="2.54" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R90"/>
<text x="-3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.27" layer="27">3X1</text>
<wire x1="-3.82" y1="-5.9" x2="-3.82" y2="-2.72" width="0.1524" layer="21"/>
<wire x1="3.82" y1="-2.72" x2="-3.82" y2="-2.72" width="0.1524" layer="21"/>
<wire x1="-3.82" y1="-5.9" x2="-2.54" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-5.9" x2="2.54" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-5.9" x2="3.82" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="3.82" y1="-2.72" x2="3.82" y2="-5.9" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="-2.54" width="0.6096" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.6096" layer="21"/>
<wire x1="-2.54" y1="-5.9" x2="-2.54" y2="-15.69" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-15.69" x2="2.54" y2="-15.69" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-15.69" x2="2.54" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.6096" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="10X04MTA" urn="urn:adsk.eagle:package:14328981/2" type="box" library_version="14">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<packageinstances>
<packageinstance name="MOLEX-KK254-4X-TOP"/>
</packageinstances>
</package3d>
<package3d name="MOLEX-KK254-4X-SIDE" urn="urn:adsk.eagle:package:14328995/1" type="model" library_version="14">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<packageinstances>
<packageinstance name="MOLEX-KK254-4X-SIDE"/>
</packageinstances>
</package3d>
<package3d name="10X02MTA" urn="urn:adsk.eagle:package:14328993/1" type="box" library_version="18">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<packageinstances>
<packageinstance name="MOLEX-KK254-2X-TOP"/>
</packageinstances>
</package3d>
<package3d name="MOLEX-KK254-2X-SIDE" urn="urn:adsk.eagle:package:14329002/1" type="model" library_version="18">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<packageinstances>
<packageinstance name="MOLEX-KK254-2X-SIDE"/>
</packageinstances>
</package3d>
<package3d name="10X03MTA" urn="urn:adsk.eagle:package:14328997/2" type="box" library_version="18">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<packageinstances>
<packageinstance name="MOLEX-KK254-3X-TOP"/>
</packageinstances>
</package3d>
<package3d name="MOLEX-KK254-3X-SIDE" urn="urn:adsk.eagle:package:14329004/1" type="model" library_version="18">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<packageinstances>
<packageinstance name="MOLEX-KK254-3X-SIDE"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MTA-1_4" urn="urn:adsk.eagle:symbol:14328942/1" library_version="14">
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="6.35" y1="-1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="6.35" y1="-1.905" x2="6.35" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.27" x2="6.35" y2="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="5.08" y="0" radius="0.635" width="0.254" layer="94"/>
<text x="7.62" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="7.62" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="-1.27" size="1.27" layer="95">1</text>
<pin name="1" x="-2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="3" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="4" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="MTA-1_2" urn="urn:adsk.eagle:symbol:14328949/1" library_version="18">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<text x="5.08" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="-1.27" size="1.27" layer="95">1</text>
<pin name="1" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="2" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="MTA-1_3" urn="urn:adsk.eagle:symbol:14328956/1" library_version="18">
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.27" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<text x="5.08" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="-1.27" size="1.27" layer="95">1</text>
<pin name="1" x="-2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="3" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOLEX-KK254-4X" urn="urn:adsk.eagle:component:14329006/2" prefix="J" uservalue="yes" library_version="14">
<description>&lt;b&gt;AMP connector&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MTA-1_4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MOLEX-KK254-4X-TOP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14328981/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDE" package="MOLEX-KK254-4X-SIDE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14328995/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOLEX-KK254-2X" urn="urn:adsk.eagle:component:14329008/1" prefix="J" uservalue="yes" library_version="18">
<description>&lt;b&gt;AMP connector&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MTA-1_2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MOLEX-KK254-2X-TOP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14328993/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDE" package="MOLEX-KK254-2X-SIDE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14329002/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOLEX-KK254-3X" urn="urn:adsk.eagle:component:14329018/2" prefix="J" uservalue="yes" library_version="18">
<description>&lt;b&gt;AMP connector&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MTA-1_3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MOLEX-KK254-3X-TOP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14328997/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDE" package="MOLEX-KK254-3X-SIDE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14329004/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ATMEGA32U4-AU">
<packages>
<package name="QFP80P1200X1200X120-44N">
<wire x1="-4.8" y1="4.8" x2="4.8" y2="4.8" width="0.2032" layer="21"/>
<wire x1="4.8" y1="4.8" x2="4.8" y2="-4.8" width="0.2032" layer="21"/>
<wire x1="4.8" y1="-4.8" x2="-4.8" y2="-4.8" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="-4.8" x2="-4.8" y2="4.8" width="0.2032" layer="21"/>
<circle x="-4" y="4" radius="0.2827" width="0.254" layer="21"/>
<text x="-7.04151875" y="7.13668125" size="1.780909375" layer="25">&gt;NAME</text>
<text x="-7.00281875" y="-9.02518125" size="1.779990625" layer="27">&gt;VALUE</text>
<rectangle x1="-6.104690625" y1="3.802959375" x2="-4.95" y2="4.1999" layer="51"/>
<rectangle x1="-6.10176875" y1="3.00081875" x2="-4.95" y2="3.4" layer="51"/>
<rectangle x1="-6.103559375" y1="2.20115" x2="-4.95" y2="2.5999" layer="51"/>
<rectangle x1="-6.10198125" y1="1.40043125" x2="-4.95" y2="1.8001" layer="51"/>
<rectangle x1="-6.109159375" y1="0.600790625" x2="-4.95" y2="1" layer="51"/>
<rectangle x1="-6.108859375" y1="-0.2001875" x2="-4.95" y2="0.1999" layer="51"/>
<rectangle x1="-6.10033125" y1="-1.000040625" x2="-4.95" y2="-0.5999" layer="51"/>
<rectangle x1="-6.10245" y1="-1.800790625" x2="-4.95" y2="-1.4" layer="51"/>
<rectangle x1="-6.110159375" y1="-2.604190625" x2="-4.95" y2="-2.1999" layer="51"/>
<rectangle x1="-6.112240625" y1="-3.406759375" x2="-4.95" y2="-3" layer="51"/>
<rectangle x1="-6.10183125" y1="-4.201090625" x2="-4.95" y2="-3.8001" layer="51"/>
<rectangle x1="-4.201559375" y1="-6.102509375" x2="-3.8001" y2="-4.95" layer="51"/>
<rectangle x1="-3.40368125" y1="-6.106690625" x2="-3" y2="-4.95" layer="51"/>
<rectangle x1="-2.60506875" y1="-6.11221875" x2="-2.1999" y2="-4.95" layer="51"/>
<rectangle x1="-1.80081875" y1="-6.10253125" x2="-1.4" y2="-4.95" layer="51"/>
<rectangle x1="-1.00156875" y1="-6.109659375" x2="-0.5999" y2="-4.95" layer="51"/>
<rectangle x1="-0.200109375" y1="-6.106440625" x2="0.1999" y2="-4.95" layer="51"/>
<rectangle x1="0.60076875" y1="-6.10893125" x2="1" y2="-4.95" layer="51"/>
<rectangle x1="1.400840625" y1="-6.103759375" x2="1.8001" y2="-4.95" layer="51"/>
<rectangle x1="2.20138125" y1="-6.104209375" x2="2.5999" y2="-4.95" layer="51"/>
<rectangle x1="3.00283125" y1="-6.105859375" x2="3.4" y2="-4.95" layer="51"/>
<rectangle x1="3.80555" y1="-6.108840625" x2="4.1999" y2="-4.95" layer="51"/>
<rectangle x1="4.95936875" y1="-4.20785" x2="6.1001" y2="-3.8001" layer="51"/>
<rectangle x1="4.950990625" y1="-3.40068125" x2="6.1001" y2="-3" layer="51"/>
<rectangle x1="4.954440625" y1="-2.60223125" x2="6.1001" y2="-2.1999" layer="51"/>
<rectangle x1="4.955909375" y1="-1.80225" x2="6.1001" y2="-1.4" layer="51"/>
<rectangle x1="4.958090625" y1="-1.00163125" x2="6.1001" y2="-0.5999" layer="51"/>
<rectangle x1="4.956659375" y1="-0.20016875" x2="6.1001" y2="0.1999" layer="51"/>
<rectangle x1="4.959759375" y1="0.601084375" x2="6.1001" y2="1" layer="51"/>
<rectangle x1="4.955159375" y1="1.401459375" x2="6.1001" y2="1.8001" layer="51"/>
<rectangle x1="4.957990625" y1="2.20345" x2="6.1001" y2="2.5999" layer="51"/>
<rectangle x1="4.956140625" y1="3.00371875" x2="6.1001" y2="3.4" layer="51"/>
<rectangle x1="4.95871875" y1="3.806790625" x2="6.1001" y2="4.1999" layer="51"/>
<rectangle x1="3.80201875" y1="4.952509375" x2="4.1999" y2="6.1001" layer="51"/>
<rectangle x1="3.001809375" y1="4.952990625" x2="3.4" y2="6.1001" layer="51"/>
<rectangle x1="2.200390625" y1="4.951109375" x2="2.5999" y2="6.1001" layer="51"/>
<rectangle x1="1.401140625" y1="4.95403125" x2="1.8001" y2="6.1001" layer="51"/>
<rectangle x1="0.600775" y1="4.95721875" x2="1" y2="6.1001" layer="51"/>
<rectangle x1="-0.200075" y1="4.954359375" x2="0.1999" y2="6.1001" layer="51"/>
<rectangle x1="-1.0019" y1="4.959390625" x2="-0.5999" y2="6.1001" layer="51"/>
<rectangle x1="-1.802209375" y1="4.955790625" x2="-1.4" y2="6.1001" layer="51"/>
<rectangle x1="-2.60418125" y1="4.95815" x2="-2.1999" y2="6.1001" layer="51"/>
<rectangle x1="-3.40398125" y1="4.955790625" x2="-3" y2="6.1001" layer="51"/>
<rectangle x1="-4.204140625" y1="4.955" x2="-3.8001" y2="6.1001" layer="51"/>
<circle x="-6.2" y="5.05" radius="0.254" width="0" layer="21"/>
<wire x1="-7" y1="7" x2="7" y2="7" width="0.05" layer="39"/>
<wire x1="7" y1="7" x2="7" y2="-7" width="0.05" layer="39"/>
<wire x1="7" y1="-7" x2="-7" y2="-7" width="0.05" layer="39"/>
<wire x1="-7" y1="-7" x2="-7" y2="7" width="0.05" layer="39"/>
<smd name="1" x="-5.9" y="4" dx="1.5" dy="0.4" layer="1"/>
<smd name="2" x="-5.9" y="3.2" dx="1.5" dy="0.4" layer="1"/>
<smd name="3" x="-5.9" y="2.4" dx="1.5" dy="0.4" layer="1"/>
<smd name="4" x="-5.9" y="1.6" dx="1.5" dy="0.4" layer="1"/>
<smd name="5" x="-5.9" y="0.8" dx="1.5" dy="0.4" layer="1"/>
<smd name="6" x="-5.9" y="0" dx="1.5" dy="0.4" layer="1"/>
<smd name="7" x="-5.9" y="-0.8" dx="1.5" dy="0.4" layer="1"/>
<smd name="8" x="-5.9" y="-1.6" dx="1.5" dy="0.4" layer="1"/>
<smd name="9" x="-5.9" y="-2.4" dx="1.5" dy="0.4" layer="1"/>
<smd name="10" x="-5.9" y="-3.2" dx="1.5" dy="0.4" layer="1"/>
<smd name="11" x="-5.9" y="-4" dx="1.5" dy="0.4" layer="1"/>
<smd name="12" x="-4" y="-5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="13" x="-3.2" y="-5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="14" x="-2.4" y="-5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="15" x="-1.6" y="-5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="16" x="-0.8" y="-5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="17" x="0" y="-5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="18" x="0.8" y="-5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="19" x="1.6" y="-5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="20" x="2.4" y="-5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="21" x="3.2" y="-5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="22" x="4" y="-5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="23" x="5.9" y="-4" dx="1.5" dy="0.4" layer="1"/>
<smd name="24" x="5.9" y="-3.2" dx="1.5" dy="0.4" layer="1"/>
<smd name="25" x="5.9" y="-2.4" dx="1.5" dy="0.4" layer="1"/>
<smd name="26" x="5.9" y="-1.6" dx="1.5" dy="0.4" layer="1"/>
<smd name="27" x="5.9" y="-0.8" dx="1.5" dy="0.4" layer="1"/>
<smd name="28" x="5.9" y="0" dx="1.5" dy="0.4" layer="1"/>
<smd name="29" x="5.9" y="0.8" dx="1.5" dy="0.4" layer="1"/>
<smd name="30" x="5.9" y="1.6" dx="1.5" dy="0.4" layer="1"/>
<smd name="31" x="5.9" y="2.4" dx="1.5" dy="0.4" layer="1"/>
<smd name="32" x="5.9" y="3.2" dx="1.5" dy="0.4" layer="1"/>
<smd name="33" x="5.9" y="4" dx="1.5" dy="0.4" layer="1"/>
<smd name="34" x="4" y="5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="35" x="3.2" y="5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="36" x="2.4" y="5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="37" x="1.6" y="5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="38" x="0.8" y="5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="39" x="0" y="5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="40" x="-0.8" y="5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="41" x="-1.6" y="5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="42" x="-2.4" y="5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="43" x="-3.2" y="5.9" dx="0.4" dy="1.5" layer="1"/>
<smd name="44" x="-4" y="5.9" dx="0.4" dy="1.5" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="ATMEGA32U4-AU">
<wire x1="10.16" y1="38.1" x2="10.16" y2="-38.1" width="0.254" layer="94"/>
<wire x1="10.16" y1="-38.1" x2="-10.16" y2="-38.1" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-38.1" x2="-10.16" y2="38.1" width="0.254" layer="94"/>
<wire x1="-10.16" y1="38.1" x2="10.16" y2="38.1" width="0.254" layer="94"/>
<text x="-10.1752" y="39.4289" size="2.5438" layer="95">&gt;NAME</text>
<text x="-10.1803" y="-41.9936" size="2.54506875" layer="96">&gt;VALUE</text>
<pin name="VCC" x="15.24" y="35.56" length="middle" direction="pwr" rot="R180"/>
<pin name="UVCC" x="15.24" y="33.02" length="middle" direction="pwr" rot="R180"/>
<pin name="AVCC" x="15.24" y="30.48" length="middle" direction="pwr" rot="R180"/>
<pin name="UGND" x="15.24" y="-33.02" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="15.24" y="-35.56" length="middle" direction="pwr" rot="R180"/>
<pin name="UCAP" x="15.24" y="-27.94" length="middle" direction="pas" rot="R180"/>
<pin name="PB0" x="-15.24" y="12.7" length="middle"/>
<pin name="PB1" x="-15.24" y="10.16" length="middle"/>
<pin name="PB2" x="-15.24" y="7.62" length="middle"/>
<pin name="PB3" x="-15.24" y="5.08" length="middle"/>
<pin name="PB4" x="-15.24" y="2.54" length="middle"/>
<pin name="PB5" x="-15.24" y="0" length="middle"/>
<pin name="PB6" x="-15.24" y="-2.54" length="middle"/>
<pin name="PB7" x="-15.24" y="-5.08" length="middle"/>
<pin name="PC6" x="15.24" y="12.7" length="middle" rot="R180"/>
<pin name="PC7" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="PD0" x="-15.24" y="-10.16" length="middle"/>
<pin name="PD1" x="-15.24" y="-12.7" length="middle"/>
<pin name="PD2" x="-15.24" y="-15.24" length="middle"/>
<pin name="PD3" x="-15.24" y="-17.78" length="middle"/>
<pin name="PD4" x="-15.24" y="-20.32" length="middle"/>
<pin name="PD5" x="-15.24" y="-22.86" length="middle"/>
<pin name="PD6" x="-15.24" y="-25.4" length="middle"/>
<pin name="PD7" x="-15.24" y="-27.94" length="middle"/>
<pin name="PE2" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="PE6" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="PF0" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="PF1" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="PF4" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="PF5" x="15.24" y="-10.16" length="middle" rot="R180"/>
<pin name="PF6" x="15.24" y="-12.7" length="middle" rot="R180"/>
<pin name="PF7" x="15.24" y="-15.24" length="middle" rot="R180"/>
<pin name="D-" x="15.24" y="-20.32" length="middle" rot="R180"/>
<pin name="D+" x="15.24" y="-22.86" length="middle" rot="R180"/>
<pin name="XTAL1" x="-15.24" y="17.78" length="middle" direction="in"/>
<pin name="!RESET" x="-15.24" y="20.32" length="middle" direction="in"/>
<pin name="AREF" x="-15.24" y="25.4" length="middle" direction="in"/>
<pin name="VBUS" x="-15.24" y="27.94" length="middle" direction="in"/>
<pin name="XTAL2" x="15.24" y="17.78" length="middle" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATMEGA32U4-AU" prefix="U">
<description>ATmega Series 16 MHz 32 KB Flash 2.5 KB SRAM 8-Bit Microcontroller - TQFP-44 &lt;a href="https://pricing.snapeda.com/parts/ATMEGA32U4-AU/Microchip/view-part?ref=eda"&gt;Check availability&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ATMEGA32U4-AU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP80P1200X1200X120-44N">
<connects>
<connect gate="G$1" pin="!RESET" pad="13"/>
<connect gate="G$1" pin="AREF" pad="42"/>
<connect gate="G$1" pin="AVCC" pad="24 44"/>
<connect gate="G$1" pin="D+" pad="4"/>
<connect gate="G$1" pin="D-" pad="3"/>
<connect gate="G$1" pin="GND" pad="15 23 35 43"/>
<connect gate="G$1" pin="PB0" pad="8"/>
<connect gate="G$1" pin="PB1" pad="9"/>
<connect gate="G$1" pin="PB2" pad="10"/>
<connect gate="G$1" pin="PB3" pad="11"/>
<connect gate="G$1" pin="PB4" pad="28"/>
<connect gate="G$1" pin="PB5" pad="29"/>
<connect gate="G$1" pin="PB6" pad="30"/>
<connect gate="G$1" pin="PB7" pad="12"/>
<connect gate="G$1" pin="PC6" pad="31"/>
<connect gate="G$1" pin="PC7" pad="32"/>
<connect gate="G$1" pin="PD0" pad="18"/>
<connect gate="G$1" pin="PD1" pad="19"/>
<connect gate="G$1" pin="PD2" pad="20"/>
<connect gate="G$1" pin="PD3" pad="21"/>
<connect gate="G$1" pin="PD4" pad="25"/>
<connect gate="G$1" pin="PD5" pad="22"/>
<connect gate="G$1" pin="PD6" pad="26"/>
<connect gate="G$1" pin="PD7" pad="27"/>
<connect gate="G$1" pin="PE2" pad="33"/>
<connect gate="G$1" pin="PE6" pad="1"/>
<connect gate="G$1" pin="PF0" pad="41"/>
<connect gate="G$1" pin="PF1" pad="40"/>
<connect gate="G$1" pin="PF4" pad="39"/>
<connect gate="G$1" pin="PF5" pad="38"/>
<connect gate="G$1" pin="PF6" pad="37"/>
<connect gate="G$1" pin="PF7" pad="36"/>
<connect gate="G$1" pin="UCAP" pad="6"/>
<connect gate="G$1" pin="UGND" pad="5"/>
<connect gate="G$1" pin="UVCC" pad="2"/>
<connect gate="G$1" pin="VBUS" pad="7"/>
<connect gate="G$1" pin="VCC" pad="14 34"/>
<connect gate="G$1" pin="XTAL1" pad="17"/>
<connect gate="G$1" pin="XTAL2" pad="16"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="In Stock"/>
<attribute name="DESCRIPTION" value=" AVR AVR® ATmega Microcontroller IC 8-Bit 16MHz 32KB (16K x 16) FLASH 44-TQFP (10x10) "/>
<attribute name="MF" value="Microchip"/>
<attribute name="MP" value="ATMEGA32U4-AU"/>
<attribute name="PACKAGE" value="TQFP-44 Microchip"/>
<attribute name="PRICE" value="None"/>
<attribute name="PURCHASE-URL" value="https://pricing.snapeda.com/search/part/ATMEGA32U4-AU/?ref=eda"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CD4051BM96">
<packages>
<package name="SOIC127P600X175-16N">
<circle x="-4.145" y="4.945" radius="0.1" width="0.2" layer="21"/>
<circle x="-4.145" y="4.945" radius="0.1" width="0.2" layer="51"/>
<wire x1="-1.95" y1="4.95" x2="1.95" y2="4.95" width="0.127" layer="51"/>
<wire x1="-1.95" y1="-4.95" x2="1.95" y2="-4.95" width="0.127" layer="51"/>
<wire x1="-1.95" y1="5.065" x2="1.95" y2="5.065" width="0.127" layer="21"/>
<wire x1="-1.95" y1="-5.065" x2="1.95" y2="-5.065" width="0.127" layer="21"/>
<wire x1="-1.95" y1="4.95" x2="-1.95" y2="-4.95" width="0.127" layer="51"/>
<wire x1="1.95" y1="4.95" x2="1.95" y2="-4.95" width="0.127" layer="51"/>
<wire x1="-3.71" y1="5.2" x2="3.71" y2="5.2" width="0.05" layer="39"/>
<wire x1="-3.71" y1="-5.2" x2="3.71" y2="-5.2" width="0.05" layer="39"/>
<wire x1="-3.71" y1="5.2" x2="-3.71" y2="-5.2" width="0.05" layer="39"/>
<wire x1="3.71" y1="5.2" x2="3.71" y2="-5.2" width="0.05" layer="39"/>
<text x="-3.97" y="-5.4" size="1.27" layer="27" align="top-left">&gt;VALUE</text>
<text x="-3.97" y="5.4" size="1.27" layer="25">&gt;NAME</text>
<smd name="1" x="-2.475" y="4.445" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="2" x="-2.475" y="3.175" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="3" x="-2.475" y="1.905" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="4" x="-2.475" y="0.635" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="5" x="-2.475" y="-0.635" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="6" x="-2.475" y="-1.905" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="7" x="-2.475" y="-3.175" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="8" x="-2.475" y="-4.445" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="9" x="2.475" y="-4.445" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="10" x="2.475" y="-3.175" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="11" x="2.475" y="-1.905" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="12" x="2.475" y="-0.635" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="13" x="2.475" y="0.635" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="14" x="2.475" y="1.905" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="15" x="2.475" y="3.175" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="16" x="2.475" y="4.445" dx="1.97" dy="0.6" layer="1" roundness="25"/>
</package>
</packages>
<symbols>
<symbol name="CD4051BM96">
<wire x1="-17.78" y1="27.94" x2="17.78" y2="27.94" width="0.254" layer="94"/>
<wire x1="17.78" y1="27.94" x2="17.78" y2="-25.4" width="0.254" layer="94"/>
<wire x1="17.78" y1="-25.4" x2="-17.78" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-25.4" x2="-17.78" y2="27.94" width="0.254" layer="94"/>
<text x="-17.78" y="28.94" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-17.78" y="-29.4" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="A" x="-22.86" y="10.16" length="middle" direction="in"/>
<pin name="B" x="-22.86" y="7.62" length="middle" direction="in"/>
<pin name="C" x="-22.86" y="5.08" length="middle" direction="in"/>
<pin name="INH" x="-22.86" y="15.24" length="middle" direction="in"/>
<pin name="CH0_IN/OUT" x="-22.86" y="0" length="middle"/>
<pin name="CH1_IN/OUT" x="-22.86" y="-2.54" length="middle"/>
<pin name="CH2_IN/OUT" x="-22.86" y="-5.08" length="middle"/>
<pin name="CH3_IN/OUT" x="-22.86" y="-7.62" length="middle"/>
<pin name="COM_OUT/IN" x="22.86" y="-7.62" length="middle" rot="R180"/>
<pin name="VDD" x="-22.86" y="25.4" length="middle" direction="pwr"/>
<pin name="CH4_IN/OUT" x="-22.86" y="-10.16" length="middle"/>
<pin name="CH5_IN/OUT" x="-22.86" y="-12.7" length="middle"/>
<pin name="CH6_IN/OUT" x="-22.86" y="-15.24" length="middle"/>
<pin name="CH7_IN/OUT" x="-22.86" y="-17.78" length="middle"/>
<pin name="VSS" x="22.86" y="-20.32" length="middle" direction="pwr" rot="R180"/>
<pin name="VEE" x="22.86" y="-22.86" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CD4051BM96" prefix="U">
<description>20-V, 8:1, 1-channel analog multiplexer with logic-level conversion 16-SOIC -55 to 125  </description>
<gates>
<gate name="G$1" symbol="CD4051BM96" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P600X175-16N">
<connects>
<connect gate="G$1" pin="A" pad="11"/>
<connect gate="G$1" pin="B" pad="10"/>
<connect gate="G$1" pin="C" pad="9"/>
<connect gate="G$1" pin="CH0_IN/OUT" pad="13"/>
<connect gate="G$1" pin="CH1_IN/OUT" pad="14"/>
<connect gate="G$1" pin="CH2_IN/OUT" pad="15"/>
<connect gate="G$1" pin="CH3_IN/OUT" pad="12"/>
<connect gate="G$1" pin="CH4_IN/OUT" pad="1"/>
<connect gate="G$1" pin="CH5_IN/OUT" pad="5"/>
<connect gate="G$1" pin="CH6_IN/OUT" pad="2"/>
<connect gate="G$1" pin="CH7_IN/OUT" pad="4"/>
<connect gate="G$1" pin="COM_OUT/IN" pad="3"/>
<connect gate="G$1" pin="INH" pad="6"/>
<connect gate="G$1" pin="VDD" pad="16"/>
<connect gate="G$1" pin="VEE" pad="7"/>
<connect gate="G$1" pin="VSS" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value=" 20-V, 8:1, 1-channel analog multiplexer with logic-level conversion "/>
<attribute name="DIGI-KEY_PART_NUMBER" value="296-14271-2-ND"/>
<attribute name="MF" value="Texas Instruments"/>
<attribute name="MP" value="CD4051BM96"/>
<attribute name="PACKAGE" value="SOIC-16 Texas Instruments"/>
<attribute name="PURCHASE-URL" value="https://pricing.snapeda.com/search/part/CD4051BM96/?ref=eda"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerSymbols" urn="urn:adsk.eagle:library:530">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND" urn="urn:adsk.eagle:symbol:39415/1" library_version="1">
<description>&lt;h3&gt;Digital Ground Supply&lt;/h3&gt;</description>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="0" y="-0.254" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:39418/1" library_version="1">
<description>&lt;h3&gt;VCC Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="VCC" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="VCC-ISO" urn="urn:adsk.eagle:symbol:39419/1" library_version="1">
<description>&lt;h3&gt;VCC Isolated Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.524" y1="1.778" x2="1.524" y2="3.302" width="0.254" layer="94"/>
<pin name="VCC-ISO" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="1.524" y="3.556" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="VCC_2" urn="urn:adsk.eagle:symbol:39423/1" library_version="1">
<description>&lt;h3&gt;VCC2 Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="VCC_2" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:39439/1" prefix="GND" library_version="1">
<description>&lt;h3&gt;Ground Supply Symbol&lt;/h3&gt;
&lt;p&gt;Generic signal ground supply symbol.&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:39449/1" prefix="SUPPLY" library_version="1">
<description>&lt;h3&gt;VCC Voltage Supply&lt;/h3&gt;
&lt;p&gt;Positive voltage supply (traditionally for a BJT device, C=collector).&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC-ISO" urn="urn:adsk.eagle:component:39443/1" library_version="1">
<description>&lt;h3&gt;Isolated power supply&lt;/h3&gt;
&lt;p&gt;Generic symbol for an isolated power supply.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="VCC-ISO" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC_2" urn="urn:adsk.eagle:component:39453/1" prefix="SUPPLY" library_version="1">
<description>&lt;h3&gt;VCC2 Voltage Supply&lt;/h3&gt;
&lt;p&gt;Secondary VCC voltage supply - Useful for a system with multiple VCC supplies.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="VCC_2" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="fab">
<packages>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1206FAB">
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206FAB">
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="USB_MINIB">
<wire x1="-0.03" y1="3.8" x2="2.07" y2="3.8" width="0.2032" layer="21"/>
<wire x1="4.57" y1="3.1" x2="4.57" y2="2.2" width="0.2032" layer="21"/>
<wire x1="4.57" y1="-2.2" x2="4.57" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="2.07" y1="-3.8" x2="-0.03" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="-4.63" y1="3.8" x2="-4.63" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-4.63" y1="-3.8" x2="-3.23" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-4.63" y1="3.8" x2="-3.23" y2="3.8" width="0.2032" layer="51"/>
<smd name="D+" x="3.77" y="0" dx="2.5" dy="0.35" layer="1"/>
<smd name="D-" x="3.77" y="0.8" dx="2.5" dy="0.35" layer="1"/>
<smd name="GND" x="3.77" y="-1.6" dx="2.5" dy="0.35" layer="1"/>
<smd name="ID" x="3.77" y="-0.8" dx="2.5" dy="0.35" layer="1"/>
<smd name="SHIELD@1" x="-1.73" y="-4.5" dx="2.5" dy="2" layer="1"/>
<smd name="SHIELD@2" x="-1.73" y="4.5" dx="2.5" dy="2" layer="1"/>
<smd name="SHIELD@4" x="3.77" y="-4.5" dx="2.5" dy="2" layer="1"/>
<smd name="SHIELD@3" x="3.77" y="4.5" dx="2.5" dy="2" layer="1"/>
<smd name="VBUS" x="3.77" y="1.6" dx="2.5" dy="0.35" layer="1"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="0" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="2-SMD-5X3MM">
<wire x1="-2.5" y1="1.3" x2="-2.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="2.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="2.5" y1="1.6" x2="2.5" y2="1.3" width="0.127" layer="21"/>
<wire x1="2.5" y1="-1.3" x2="2.5" y2="-1.6" width="0.127" layer="21"/>
<wire x1="2.5" y1="-1.6" x2="-2.5" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-1.6" x2="-2.5" y2="-1.3" width="0.127" layer="21"/>
<smd name="P$1" x="-2" y="0" dx="2" dy="2.4" layer="1"/>
<smd name="P$2" x="2" y="0" dx="2" dy="2.4" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SOD123">
<description>&lt;b&gt;SMALL OUTLINE DIODE&lt;/b&gt;</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.321" y1="0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="1.321" y2="-0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="-1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="1.321" y1="-0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1" y1="0" x2="0" y2="0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="0.5" x2="0" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="-0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="CATHODE" x="-1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<smd name="ANODE" x="1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.9558" y1="-0.3048" x2="-1.3716" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="1.3716" y1="-0.3048" x2="1.9558" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="CAP-NONPOLARIZED">
<description>non-polarized capacitor</description>
<wire x1="-1.778" y1="1.524" x2="-1.778" y2="0" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0" x2="-1.778" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.762" y1="1.524" x2="-0.762" y2="0" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="USB">
<wire x1="5.08" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="3.81" y="-5.08" size="2.54" layer="94" rot="R90">USB</text>
<pin name="D+" x="-2.54" y="2.54" visible="pad" length="short"/>
<pin name="D-" x="-2.54" y="0" visible="pad" length="short"/>
<pin name="VBUS" x="-2.54" y="-2.54" visible="pad" length="short"/>
<pin name="GND" x="-2.54" y="-5.08" visible="pad" length="short"/>
<pin name="SHIELD" x="2.54" y="-10.16" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="CRYSTAL">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="0" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="-1.016" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.016" y="-4.318" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP-UNPOLARIZED" prefix="C" uservalue="yes">
<gates>
<gate name="&gt;NAME" symbol="CAP-NONPOLARIZED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C1206">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FAB" package="C1206FAB">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES-US" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor (US Symbol)&lt;/b&gt;
&lt;p&gt;
Variants with postfix FAB are widened to allow the routing of internal traces</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206FAB" package="R1206FAB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB_MINIB">
<gates>
<gate name="G$1" symbol="USB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="USB_MINIB">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="SHIELD" pad="SHIELD@1 SHIELD@2 SHIELD@3 SHIELD@4"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL">
<gates>
<gate name="G$1" symbol="CRYSTAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2-SMD-5X3MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
high speed (Philips)</description>
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="SOD123" package="SOD123">
<connects>
<connect gate="G$1" pin="A" pad="ANODE"/>
<connect gate="G$1" pin="C" pad="CATHODE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="AG">
<packages>
<package name="PRS11R-4XXF-SXXXXX">
<wire x1="-0.3" y1="0.7" x2="-0.3" y2="13.8" width="0.5" layer="51"/>
<wire x1="-0.3" y1="13.8" x2="11.4" y2="13.8" width="0.5" layer="51"/>
<wire x1="11.4" y1="13.8" x2="11.4" y2="0.7" width="0.5" layer="51"/>
<wire x1="11.4" y1="0.7" x2="-0.3" y2="0.7" width="0.5" layer="51"/>
<pad name="2" x="5.55" y="0.7" drill="1.1" diameter="1.5" shape="square"/>
<pad name="1" x="3.05" y="0.7" drill="1.1" diameter="1.5" shape="square"/>
<pad name="3" x="8.05" y="0.7" drill="1.1" diameter="1.5" shape="square"/>
<pad name="4" x="3.05" y="14.5" drill="1.1" diameter="1.5" shape="square"/>
<pad name="5" x="8.05" y="14.5" drill="1.1" diameter="1.5" shape="square"/>
<wire x1="-0.9" y1="9.5" x2="0.9" y2="9.5" width="0.127" layer="20"/>
<wire x1="0.9" y1="9.5" x2="0.9" y2="6.9" width="0.127" layer="20"/>
<wire x1="0.9" y1="6.9" x2="-0.9" y2="6.9" width="0.127" layer="20"/>
<wire x1="-0.9" y1="6.9" x2="-0.9" y2="9.5" width="0.127" layer="20"/>
<wire x1="10.2" y1="6.9" x2="10.2" y2="9.5" width="0.127" layer="20"/>
<wire x1="10.2" y1="9.5" x2="12" y2="9.5" width="0.127" layer="20"/>
<wire x1="12" y1="6.9" x2="10.2" y2="6.9" width="0.127" layer="20"/>
<wire x1="12" y1="9.5" x2="12" y2="6.9" width="0.127" layer="20"/>
<rectangle x1="0" y1="0" x2="11.1" y2="14.5" layer="29"/>
<text x="1.7" y="7.5" size="1.778" layer="25">&gt;name</text>
<rectangle x1="-1.27" y1="6.35" x2="1.27" y2="10.16" layer="1"/>
<rectangle x1="9.851896875" y1="6.35" x2="12.391896875" y2="10.16" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="PRS11R-SWITCH">
<wire x1="0" y1="0" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="15.24" y2="20.32" width="0.254" layer="94"/>
<wire x1="15.24" y1="20.32" x2="0" y2="20.32" width="0.254" layer="94"/>
<wire x1="0" y1="20.32" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="17.78" length="short"/>
<pin name="2" x="-2.54" y="15.24" length="short" direction="out"/>
<pin name="3" x="-2.54" y="12.7" length="short"/>
<pin name="5" x="-2.54" y="2.54" length="short"/>
<pin name="4" x="-2.54" y="5.08" length="short"/>
<text x="0.254" y="20.828" size="1.778" layer="95">G$1</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PRS11R-4XXF-SXXXXX">
<gates>
<gate name="G$1" symbol="PRS11R-SWITCH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PRS11R-4XXF-SXXXXX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.3048" drill="0">
<clearance class="0" value="0.39"/>
</class>
<class number="1" name="wider" width="0.4064" drill="0">
<clearance class="1" value="0.39"/>
</class>
</classes>
<parts>
<part name="J1" library="00-DDD-CONNECTORS" library_urn="urn:adsk.eagle:library:14328941" deviceset="MOLEX-KK254-4X" device="" package3d_urn="urn:adsk.eagle:package:14328981/2"/>
<part name="J2" library="00-DDD-CONNECTORS" library_urn="urn:adsk.eagle:library:14328941" deviceset="MOLEX-KK254-4X" device="" package3d_urn="urn:adsk.eagle:package:14328981/2"/>
<part name="U1" library="ATMEGA32U4-AU" deviceset="ATMEGA32U4-AU" device=""/>
<part name="STEPS_0-7" library="CD4051BM96" deviceset="CD4051BM96" device=""/>
<part name="GND1" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="SUPPLY1" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device=""/>
<part name="C1" library="fab" deviceset="CAP-UNPOLARIZED" device="FAB" value="10uF"/>
<part name="J5" library="00-DDD-CONNECTORS" library_urn="urn:adsk.eagle:library:14328941" deviceset="MOLEX-KK254-2X" device="" package3d_urn="urn:adsk.eagle:package:14328993/1"/>
<part name="U$1" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC-ISO" device=""/>
<part name="U$2" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC-ISO" device=""/>
<part name="U$3" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC-ISO" device=""/>
<part name="GND2" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND3" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND4" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND5" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="STEPS_8-15" library="CD4051BM96" deviceset="CD4051BM96" device=""/>
<part name="U$7" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC-ISO" device=""/>
<part name="GND6" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND7" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="R2" library="fab" deviceset="RES-US" device="1206FAB" value="22"/>
<part name="R3" library="fab" deviceset="RES-US" device="1206FAB" value="22"/>
<part name="U$8" library="fab" deviceset="USB_MINIB" device=""/>
<part name="U$5" library="fab" deviceset="CRYSTAL" device=""/>
<part name="GND8" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="C2" library="fab" deviceset="CAP-UNPOLARIZED" device="FAB" value="18pF"/>
<part name="C3" library="fab" deviceset="CAP-UNPOLARIZED" device="FAB" value="18pF"/>
<part name="C4" library="fab" deviceset="CAP-UNPOLARIZED" device="FAB" value="1uF"/>
<part name="C5" library="fab" deviceset="CAP-UNPOLARIZED" device="FAB" value="100nF"/>
<part name="POTS_0-7" library="CD4051BM96" deviceset="CD4051BM96" device=""/>
<part name="U$6" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC-ISO" device=""/>
<part name="GND10" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND11" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="POTS_8-15" library="CD4051BM96" deviceset="CD4051BM96" device=""/>
<part name="U$10" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC-ISO" device=""/>
<part name="GND12" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND13" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="D1" library="fab" deviceset="DIODE" device="SOD123"/>
<part name="SUPPLY2" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC_2" device=""/>
<part name="J6" library="00-DDD-CONNECTORS" library_urn="urn:adsk.eagle:library:14328941" deviceset="MOLEX-KK254-4X" device="" package3d_urn="urn:adsk.eagle:package:14328981/2"/>
<part name="J7" library="00-DDD-CONNECTORS" library_urn="urn:adsk.eagle:library:14328941" deviceset="MOLEX-KK254-4X" device="" package3d_urn="urn:adsk.eagle:package:14328981/2"/>
<part name="J8" library="00-DDD-CONNECTORS" library_urn="urn:adsk.eagle:library:14328941" deviceset="MOLEX-KK254-4X" device="" package3d_urn="urn:adsk.eagle:package:14328981/2"/>
<part name="J9" library="00-DDD-CONNECTORS" library_urn="urn:adsk.eagle:library:14328941" deviceset="MOLEX-KK254-4X" device="" package3d_urn="urn:adsk.eagle:package:14328981/2"/>
<part name="J10" library="00-DDD-CONNECTORS" library_urn="urn:adsk.eagle:library:14328941" deviceset="MOLEX-KK254-4X" device="" package3d_urn="urn:adsk.eagle:package:14328981/2"/>
<part name="J11" library="00-DDD-CONNECTORS" library_urn="urn:adsk.eagle:library:14328941" deviceset="MOLEX-KK254-4X" device="" package3d_urn="urn:adsk.eagle:package:14328981/2"/>
<part name="J12" library="00-DDD-CONNECTORS" library_urn="urn:adsk.eagle:library:14328941" deviceset="MOLEX-KK254-3X" device="" package3d_urn="urn:adsk.eagle:package:14328997/2"/>
<part name="C6" library="fab" deviceset="CAP-UNPOLARIZED" device="FAB" value="1000uF"/>
<part name="R4" library="fab" deviceset="RES-US" device="1206FAB" value="470"/>
<part name="SUPPLY3" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC_2" device=""/>
<part name="GND9" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="U$9" library="AG" deviceset="PRS11R-4XXF-SXXXXX" device=""/>
<part name="U$12" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC-ISO" device=""/>
<part name="GND14" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="R5" library="fab" deviceset="RES-US" device="1206FAB" value="10k"/>
<part name="U$11" library="AG" deviceset="PRS11R-4XXF-SXXXXX" device=""/>
<part name="U$13" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC-ISO" device=""/>
<part name="GND15" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="R6" library="fab" deviceset="RES-US" device="1206FAB" value="10k"/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="2.54" y1="162.56" x2="45.72" y2="162.56" width="0.1524" layer="150"/>
<wire x1="45.72" y1="162.56" x2="45.72" y2="124.46" width="0.1524" layer="150"/>
<wire x1="45.72" y1="124.46" x2="2.54" y2="124.46" width="0.1524" layer="150"/>
<wire x1="2.54" y1="124.46" x2="2.54" y2="162.56" width="0.1524" layer="150"/>
<text x="20.32" y="157.48" size="1.778" layer="150">Crystal</text>
<text x="63.5" y="157.48" size="1.778" layer="150">Power supply</text>
<wire x1="45.72" y1="162.56" x2="93.98" y2="162.56" width="0.1524" layer="150"/>
<wire x1="93.98" y1="162.56" x2="93.98" y2="124.46" width="0.1524" layer="150"/>
<wire x1="93.98" y1="124.46" x2="45.72" y2="124.46" width="0.1524" layer="150"/>
<text x="106.68" y="157.48" size="1.778" layer="150">USB connector</text>
<wire x1="93.98" y1="162.56" x2="132.08" y2="162.56" width="0.1524" layer="150"/>
<wire x1="132.08" y1="162.56" x2="132.08" y2="124.46" width="0.1524" layer="150"/>
<wire x1="132.08" y1="124.46" x2="93.98" y2="124.46" width="0.1524" layer="150"/>
<wire x1="132.08" y1="162.56" x2="294.64" y2="162.56" width="0.1524" layer="150"/>
<wire x1="294.64" y1="162.56" x2="294.64" y2="7.62" width="0.1524" layer="150"/>
<wire x1="294.64" y1="7.62" x2="132.08" y2="7.62" width="0.1524" layer="150"/>
<wire x1="132.08" y1="7.62" x2="132.08" y2="124.46" width="0.1524" layer="150"/>
<text x="208.28" y="157.48" size="1.778" layer="150">Multiplexers</text>
<text x="-25.4" y="157.48" size="1.778" layer="150">LED connector</text>
<wire x1="2.54" y1="162.56" x2="-38.1" y2="162.56" width="0.1524" layer="150"/>
<wire x1="-38.1" y1="162.56" x2="-38.1" y2="124.46" width="0.1524" layer="150"/>
<wire x1="-38.1" y1="124.46" x2="2.54" y2="124.46" width="0.1524" layer="150"/>
<text x="101.6" y="121.92" size="1.778" layer="97">General Potentiometers</text>
<wire x1="93.98" y1="124.46" x2="93.98" y2="45.72" width="0.1524" layer="97"/>
<wire x1="93.98" y1="45.72" x2="132.08" y2="45.72" width="0.1524" layer="97"/>
</plain>
<instances>
<instance part="J1" gate="G$1" x="147.32" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="147.32" y="119.38" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="151.13" y="119.38" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J2" gate="G$1" x="147.32" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="147.32" y="109.22" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="151.13" y="109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U1" gate="G$1" x="20.32" y="68.58" smashed="yes">
<attribute name="NAME" x="10.1448" y="108.0089" size="2.5438" layer="95"/>
<attribute name="VALUE" x="10.1397" y="26.5864" size="2.54506875" layer="96"/>
</instance>
<instance part="STEPS_0-7" gate="G$1" x="175.26" y="116.84" smashed="yes">
<attribute name="NAME" x="157.48" y="145.78" size="2.0828" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="157.48" y="87.44" size="2.0828" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="GND1" gate="1" x="81.28" y="129.54" smashed="yes">
<attribute name="VALUE" x="81.28" y="129.286" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="SUPPLY1" gate="G$1" x="63.5" y="149.86" smashed="yes">
<attribute name="VALUE" x="63.5" y="152.654" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="C1" gate="&gt;NAME" x="109.22" y="139.7" smashed="yes" rot="R270">
<attribute name="NAME" x="111.76" y="143.51" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="105.41" y="143.51" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J5" gate="G$1" x="58.42" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="58.42" y="149.86" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="62.23" y="149.86" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U$1" gate="G$1" x="66.04" y="137.16" smashed="yes">
<attribute name="VALUE" x="67.564" y="140.716" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="U$2" gate="G$1" x="149.86" y="144.78" smashed="yes">
<attribute name="VALUE" x="151.384" y="148.336" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="U$3" gate="G$1" x="40.64" y="106.68" smashed="yes">
<attribute name="VALUE" x="42.164" y="110.236" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND2" gate="1" x="200.66" y="88.9" smashed="yes">
<attribute name="VALUE" x="200.66" y="88.646" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND3" gate="1" x="38.1" y="27.94" smashed="yes">
<attribute name="VALUE" x="38.1" y="27.686" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND4" gate="1" x="149.86" y="128.016" smashed="yes">
<attribute name="VALUE" x="149.86" y="127.762" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND5" gate="1" x="119.38" y="129.54" smashed="yes">
<attribute name="VALUE" x="119.38" y="129.286" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="STEPS_8-15" gate="G$1" x="251.46" y="116.84" smashed="yes">
<attribute name="NAME" x="233.68" y="145.78" size="2.0828" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="233.68" y="87.44" size="2.0828" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="U$7" gate="G$1" x="226.06" y="144.78" smashed="yes">
<attribute name="VALUE" x="227.584" y="148.336" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND6" gate="1" x="276.86" y="88.9" smashed="yes">
<attribute name="VALUE" x="276.86" y="88.646" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND7" gate="1" x="226.06" y="128.016" smashed="yes">
<attribute name="VALUE" x="226.06" y="127.762" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R2" gate="G$1" x="43.18" y="48.26" smashed="yes">
<attribute name="NAME" x="39.37" y="49.7586" size="1.778" layer="95"/>
<attribute name="VALUE" x="39.37" y="44.958" size="1.778" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="43.18" y="45.72" smashed="yes">
<attribute name="NAME" x="39.37" y="47.2186" size="1.778" layer="95"/>
<attribute name="VALUE" x="39.37" y="42.418" size="1.778" layer="96"/>
</instance>
<instance part="U$8" gate="G$1" x="116.84" y="149.86" smashed="yes"/>
<instance part="U$5" gate="G$1" x="20.32" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="17.78" y="141.224" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="24.638" y="141.224" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND8" gate="1" x="33.02" y="132.08" smashed="yes">
<attribute name="VALUE" x="33.02" y="131.826" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C2" gate="&gt;NAME" x="27.94" y="147.32" smashed="yes">
<attribute name="NAME" x="24.13" y="149.86" size="1.778" layer="95"/>
<attribute name="VALUE" x="24.13" y="143.51" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="&gt;NAME" x="27.94" y="137.16" smashed="yes">
<attribute name="NAME" x="24.13" y="139.7" size="1.778" layer="95"/>
<attribute name="VALUE" x="24.13" y="133.35" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="&gt;NAME" x="76.2" y="142.24" smashed="yes">
<attribute name="NAME" x="72.39" y="144.78" size="1.778" layer="95"/>
<attribute name="VALUE" x="72.39" y="138.43" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="&gt;NAME" x="76.2" y="134.62" smashed="yes">
<attribute name="NAME" x="72.39" y="137.16" size="1.778" layer="95"/>
<attribute name="VALUE" x="72.39" y="130.81" size="1.778" layer="96"/>
</instance>
<instance part="POTS_0-7" gate="G$1" x="175.26" y="48.26" smashed="yes">
<attribute name="NAME" x="157.48" y="77.2" size="2.0828" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="157.48" y="18.86" size="2.0828" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="U$6" gate="G$1" x="149.86" y="76.2" smashed="yes">
<attribute name="VALUE" x="151.384" y="79.756" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND10" gate="1" x="200.66" y="20.32" smashed="yes">
<attribute name="VALUE" x="200.66" y="20.066" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND11" gate="1" x="149.86" y="59.436" smashed="yes">
<attribute name="VALUE" x="149.86" y="59.182" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="POTS_8-15" gate="G$1" x="251.46" y="48.26" smashed="yes">
<attribute name="NAME" x="233.68" y="77.2" size="2.0828" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="233.68" y="18.86" size="2.0828" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="U$10" gate="G$1" x="226.06" y="76.2" smashed="yes">
<attribute name="VALUE" x="227.584" y="79.756" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND12" gate="1" x="276.86" y="20.32" smashed="yes">
<attribute name="VALUE" x="276.86" y="20.066" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND13" gate="1" x="226.06" y="59.436" smashed="yes">
<attribute name="VALUE" x="226.06" y="59.182" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="D1" gate="G$1" x="63.5" y="139.7" smashed="yes" rot="MR270">
<attribute name="NAME" x="63.0174" y="137.16" size="1.778" layer="95" rot="MR270"/>
<attribute name="VALUE" x="65.8114" y="137.16" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="SUPPLY2" gate="G$1" x="66.04" y="147.32" smashed="yes">
<attribute name="VALUE" x="66.04" y="150.114" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="J6" gate="G$1" x="223.52" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="223.52" y="119.38" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="227.33" y="119.38" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J7" gate="G$1" x="223.52" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="223.52" y="109.22" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="227.33" y="109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J8" gate="G$1" x="223.52" y="43.18" smashed="yes" rot="R90">
<attribute name="NAME" x="223.52" y="50.8" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="227.33" y="50.8" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J9" gate="G$1" x="223.52" y="33.02" smashed="yes" rot="R90">
<attribute name="NAME" x="223.52" y="40.64" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="227.33" y="40.64" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J10" gate="G$1" x="147.32" y="43.18" smashed="yes" rot="R90">
<attribute name="NAME" x="147.32" y="50.8" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="151.13" y="50.8" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J11" gate="G$1" x="147.32" y="33.02" smashed="yes" rot="R90">
<attribute name="NAME" x="147.32" y="40.64" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="151.13" y="40.64" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J12" gate="G$1" x="-22.86" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="-22.86" y="147.32" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-19.05" y="147.32" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C6" gate="&gt;NAME" x="-22.86" y="134.62" smashed="yes">
<attribute name="NAME" x="-26.67" y="137.16" size="1.778" layer="95"/>
<attribute name="VALUE" x="-26.67" y="130.81" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="-12.7" y="142.24" smashed="yes">
<attribute name="NAME" x="-16.51" y="143.7386" size="1.778" layer="95"/>
<attribute name="VALUE" x="-16.51" y="138.938" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY3" gate="G$1" x="-17.78" y="149.86" smashed="yes">
<attribute name="VALUE" x="-17.78" y="152.654" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND9" gate="1" x="-17.78" y="129.54" smashed="yes">
<attribute name="VALUE" x="-17.78" y="129.286" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="U$9" gate="G$1" x="111.76" y="91.44" smashed="yes"/>
<instance part="U$12" gate="G$1" x="106.68" y="111.76" smashed="yes">
<attribute name="VALUE" x="108.204" y="115.316" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND14" gate="1" x="106.68" y="88.9" smashed="yes">
<attribute name="VALUE" x="106.68" y="88.646" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R5" gate="G$1" x="101.6" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="100.1014" y="97.79" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="104.902" y="97.79" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U$11" gate="G$1" x="111.76" y="55.88" smashed="yes"/>
<instance part="U$13" gate="G$1" x="106.68" y="76.2" smashed="yes">
<attribute name="VALUE" x="108.204" y="79.756" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND15" gate="1" x="106.68" y="53.34" smashed="yes">
<attribute name="VALUE" x="106.68" y="53.086" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R6" gate="G$1" x="101.6" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="100.1014" y="62.23" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="104.902" y="62.23" size="1.778" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="1">
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="C5" gate="&gt;NAME" pin="2"/>
<wire x1="78.74" y1="134.62" x2="81.28" y2="134.62" width="0.1524" layer="91"/>
<pinref part="C4" gate="&gt;NAME" pin="2"/>
<wire x1="81.28" y1="134.62" x2="81.28" y2="132.08" width="0.1524" layer="91"/>
<wire x1="78.74" y1="142.24" x2="81.28" y2="142.24" width="0.1524" layer="91"/>
<wire x1="81.28" y1="142.24" x2="81.28" y2="134.62" width="0.1524" layer="91"/>
<junction x="81.28" y="134.62"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="UGND"/>
<wire x1="35.56" y1="35.56" x2="38.1" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="38.1" y1="35.56" x2="38.1" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="38.1" y1="33.02" x2="38.1" y2="30.48" width="0.1524" layer="91"/>
<wire x1="35.56" y1="33.02" x2="38.1" y2="33.02" width="0.1524" layer="91"/>
<junction x="38.1" y="33.02"/>
</segment>
<segment>
<pinref part="STEPS_0-7" gate="G$1" pin="INH"/>
<wire x1="152.4" y1="132.08" x2="149.86" y2="132.08" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="149.86" y1="132.08" x2="149.86" y2="130.556" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="STEPS_0-7" gate="G$1" pin="VSS"/>
<wire x1="198.12" y1="96.52" x2="200.66" y2="96.52" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="200.66" y1="96.52" x2="200.66" y2="93.98" width="0.1524" layer="91"/>
<pinref part="STEPS_0-7" gate="G$1" pin="VEE"/>
<wire x1="200.66" y1="93.98" x2="200.66" y2="91.44" width="0.1524" layer="91"/>
<wire x1="198.12" y1="93.98" x2="200.66" y2="93.98" width="0.1524" layer="91"/>
<junction x="200.66" y="93.98"/>
</segment>
<segment>
<pinref part="STEPS_8-15" gate="G$1" pin="INH"/>
<wire x1="228.6" y1="132.08" x2="226.06" y2="132.08" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="226.06" y1="132.08" x2="226.06" y2="130.556" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="STEPS_8-15" gate="G$1" pin="VSS"/>
<wire x1="274.32" y1="96.52" x2="276.86" y2="96.52" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="276.86" y1="96.52" x2="276.86" y2="93.98" width="0.1524" layer="91"/>
<pinref part="STEPS_8-15" gate="G$1" pin="VEE"/>
<wire x1="276.86" y1="93.98" x2="276.86" y2="91.44" width="0.1524" layer="91"/>
<wire x1="274.32" y1="93.98" x2="276.86" y2="93.98" width="0.1524" layer="91"/>
<junction x="276.86" y="93.98"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="GND"/>
<wire x1="114.3" y1="144.78" x2="111.76" y2="144.78" width="0.1524" layer="91"/>
<wire x1="111.76" y1="144.78" x2="111.76" y2="134.62" width="0.1524" layer="91"/>
<wire x1="111.76" y1="134.62" x2="119.38" y2="134.62" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="SHIELD"/>
<wire x1="119.38" y1="134.62" x2="119.38" y2="139.7" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="119.38" y1="132.08" x2="119.38" y2="134.62" width="0.1524" layer="91"/>
<junction x="119.38" y="134.62"/>
<pinref part="C1" gate="&gt;NAME" pin="2"/>
<wire x1="109.22" y1="137.16" x2="109.22" y2="134.62" width="0.1524" layer="91"/>
<wire x1="109.22" y1="134.62" x2="111.76" y2="134.62" width="0.1524" layer="91"/>
<junction x="111.76" y="134.62"/>
</segment>
<segment>
<pinref part="C2" gate="&gt;NAME" pin="2"/>
<wire x1="30.48" y1="147.32" x2="33.02" y2="147.32" width="0.1524" layer="91"/>
<wire x1="33.02" y1="147.32" x2="33.02" y2="137.16" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
<pinref part="C3" gate="&gt;NAME" pin="2"/>
<wire x1="33.02" y1="137.16" x2="33.02" y2="134.62" width="0.1524" layer="91"/>
<wire x1="30.48" y1="137.16" x2="33.02" y2="137.16" width="0.1524" layer="91"/>
<junction x="33.02" y="137.16"/>
</segment>
<segment>
<pinref part="POTS_0-7" gate="G$1" pin="INH"/>
<wire x1="152.4" y1="63.5" x2="149.86" y2="63.5" width="0.1524" layer="91"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="149.86" y1="63.5" x2="149.86" y2="61.976" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="POTS_0-7" gate="G$1" pin="VSS"/>
<wire x1="198.12" y1="27.94" x2="200.66" y2="27.94" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="200.66" y1="27.94" x2="200.66" y2="25.4" width="0.1524" layer="91"/>
<pinref part="POTS_0-7" gate="G$1" pin="VEE"/>
<wire x1="200.66" y1="25.4" x2="200.66" y2="22.86" width="0.1524" layer="91"/>
<wire x1="198.12" y1="25.4" x2="200.66" y2="25.4" width="0.1524" layer="91"/>
<junction x="200.66" y="25.4"/>
</segment>
<segment>
<pinref part="POTS_8-15" gate="G$1" pin="INH"/>
<wire x1="228.6" y1="63.5" x2="226.06" y2="63.5" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="226.06" y1="63.5" x2="226.06" y2="61.976" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="POTS_8-15" gate="G$1" pin="VSS"/>
<wire x1="274.32" y1="27.94" x2="276.86" y2="27.94" width="0.1524" layer="91"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="276.86" y1="27.94" x2="276.86" y2="25.4" width="0.1524" layer="91"/>
<pinref part="POTS_8-15" gate="G$1" pin="VEE"/>
<wire x1="276.86" y1="25.4" x2="276.86" y2="22.86" width="0.1524" layer="91"/>
<wire x1="274.32" y1="25.4" x2="276.86" y2="25.4" width="0.1524" layer="91"/>
<junction x="276.86" y="25.4"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="139.7" x2="-17.78" y2="139.7" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="139.7" x2="-17.78" y2="134.62" width="0.1524" layer="91"/>
<pinref part="C6" gate="&gt;NAME" pin="2"/>
<wire x1="-17.78" y1="134.62" x2="-20.32" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="134.62" x2="-17.78" y2="132.08" width="0.1524" layer="91"/>
<junction x="-17.78" y="134.62"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$9" gate="G$1" pin="3"/>
<wire x1="109.22" y1="104.14" x2="106.68" y2="104.14" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="106.68" y1="104.14" x2="106.68" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U$9" gate="G$1" pin="4"/>
<wire x1="106.68" y1="96.52" x2="106.68" y2="91.44" width="0.1524" layer="91"/>
<wire x1="109.22" y1="96.52" x2="106.68" y2="96.52" width="0.1524" layer="91"/>
<junction x="106.68" y="96.52"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="3"/>
<wire x1="109.22" y1="68.58" x2="106.68" y2="68.58" width="0.1524" layer="91"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="106.68" y1="68.58" x2="106.68" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U$11" gate="G$1" pin="4"/>
<wire x1="106.68" y1="60.96" x2="106.68" y2="55.88" width="0.1524" layer="91"/>
<wire x1="109.22" y1="60.96" x2="106.68" y2="60.96" width="0.1524" layer="91"/>
<junction x="106.68" y="60.96"/>
</segment>
</net>
<net name="VCC-ISO" class="1">
<segment>
<pinref part="U$3" gate="G$1" pin="VCC-ISO"/>
<pinref part="U1" gate="G$1" pin="AVCC"/>
<wire x1="35.56" y1="99.06" x2="40.64" y2="99.06" width="0.1524" layer="91"/>
<wire x1="40.64" y1="99.06" x2="40.64" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VCC"/>
<wire x1="40.64" y1="104.14" x2="40.64" y2="106.68" width="0.1524" layer="91"/>
<wire x1="35.56" y1="104.14" x2="40.64" y2="104.14" width="0.1524" layer="91"/>
<junction x="40.64" y="104.14"/>
</segment>
<segment>
<pinref part="STEPS_0-7" gate="G$1" pin="VDD"/>
<wire x1="152.4" y1="142.24" x2="149.86" y2="142.24" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="VCC-ISO"/>
<wire x1="149.86" y1="142.24" x2="149.86" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="STEPS_8-15" gate="G$1" pin="VDD"/>
<wire x1="228.6" y1="142.24" x2="226.06" y2="142.24" width="0.1524" layer="91"/>
<pinref part="U$7" gate="G$1" pin="VCC-ISO"/>
<wire x1="226.06" y1="142.24" x2="226.06" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="UCAP"/>
<wire x1="35.56" y1="40.64" x2="38.1" y2="40.64" width="0.1524" layer="91"/>
<label x="38.1" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="POTS_0-7" gate="G$1" pin="VDD"/>
<wire x1="152.4" y1="73.66" x2="149.86" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="VCC-ISO"/>
<wire x1="149.86" y1="73.66" x2="149.86" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="POTS_8-15" gate="G$1" pin="VDD"/>
<wire x1="228.6" y1="73.66" x2="226.06" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U$10" gate="G$1" pin="VCC-ISO"/>
<wire x1="226.06" y1="73.66" x2="226.06" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="63.5" y1="137.16" x2="63.5" y2="134.62" width="0.1524" layer="91"/>
<wire x1="63.5" y1="134.62" x2="66.04" y2="134.62" width="0.1524" layer="91"/>
<pinref part="C5" gate="&gt;NAME" pin="1"/>
<pinref part="U$1" gate="G$1" pin="VCC-ISO"/>
<wire x1="66.04" y1="134.62" x2="68.58" y2="134.62" width="0.1524" layer="91"/>
<wire x1="68.58" y1="134.62" x2="71.12" y2="134.62" width="0.1524" layer="91"/>
<wire x1="66.04" y1="137.16" x2="66.04" y2="134.62" width="0.1524" layer="91"/>
<junction x="66.04" y="134.62"/>
<pinref part="C4" gate="&gt;NAME" pin="1"/>
<wire x1="71.12" y1="142.24" x2="68.58" y2="142.24" width="0.1524" layer="91"/>
<wire x1="68.58" y1="142.24" x2="68.58" y2="134.62" width="0.1524" layer="91"/>
<junction x="68.58" y="134.62"/>
</segment>
<segment>
<pinref part="U$9" gate="G$1" pin="1"/>
<wire x1="109.22" y1="109.22" x2="106.68" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="VCC-ISO"/>
<wire x1="106.68" y1="109.22" x2="106.68" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="101.6" y1="106.68" x2="101.6" y2="109.22" width="0.1524" layer="91"/>
<wire x1="101.6" y1="109.22" x2="106.68" y2="109.22" width="0.1524" layer="91"/>
<junction x="106.68" y="109.22"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="1"/>
<wire x1="109.22" y1="73.66" x2="106.68" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U$13" gate="G$1" pin="VCC-ISO"/>
<wire x1="106.68" y1="73.66" x2="106.68" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="101.6" y1="71.12" x2="101.6" y2="73.66" width="0.1524" layer="91"/>
<wire x1="101.6" y1="73.66" x2="106.68" y2="73.66" width="0.1524" layer="91"/>
<junction x="106.68" y="73.66"/>
</segment>
</net>
<net name="MPX_A" class="0">
<segment>
<pinref part="STEPS_0-7" gate="G$1" pin="A"/>
<wire x1="152.4" y1="127" x2="149.86" y2="127" width="0.1524" layer="91"/>
<label x="149.86" y="127" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="228.6" y1="127" x2="226.06" y2="127" width="0.1524" layer="91"/>
<label x="226.06" y="127" size="1.778" layer="95" rot="R180"/>
<pinref part="STEPS_8-15" gate="G$1" pin="A"/>
</segment>
<segment>
<wire x1="152.4" y1="58.42" x2="149.86" y2="58.42" width="0.1524" layer="91"/>
<label x="149.86" y="58.42" size="1.778" layer="95" rot="R180"/>
<pinref part="POTS_0-7" gate="G$1" pin="A"/>
</segment>
<segment>
<wire x1="228.6" y1="58.42" x2="226.06" y2="58.42" width="0.1524" layer="91"/>
<label x="226.06" y="58.42" size="1.778" layer="95" rot="R180"/>
<pinref part="POTS_8-15" gate="G$1" pin="A"/>
</segment>
<segment>
<wire x1="5.08" y1="81.28" x2="2.54" y2="81.28" width="0.1524" layer="91"/>
<label x="2.54" y="81.28" size="1.778" layer="95" rot="R180"/>
<pinref part="U1" gate="G$1" pin="PB0"/>
</segment>
</net>
<net name="MPX_B" class="0">
<segment>
<pinref part="STEPS_0-7" gate="G$1" pin="B"/>
<wire x1="152.4" y1="124.46" x2="149.86" y2="124.46" width="0.1524" layer="91"/>
<label x="149.86" y="124.46" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="228.6" y1="124.46" x2="226.06" y2="124.46" width="0.1524" layer="91"/>
<label x="226.06" y="124.46" size="1.778" layer="95" rot="R180"/>
<pinref part="STEPS_8-15" gate="G$1" pin="B"/>
</segment>
<segment>
<wire x1="152.4" y1="55.88" x2="149.86" y2="55.88" width="0.1524" layer="91"/>
<label x="149.86" y="55.88" size="1.778" layer="95" rot="R180"/>
<pinref part="POTS_0-7" gate="G$1" pin="B"/>
</segment>
<segment>
<wire x1="228.6" y1="55.88" x2="226.06" y2="55.88" width="0.1524" layer="91"/>
<label x="226.06" y="55.88" size="1.778" layer="95" rot="R180"/>
<pinref part="POTS_8-15" gate="G$1" pin="B"/>
</segment>
<segment>
<wire x1="5.08" y1="78.74" x2="2.54" y2="78.74" width="0.1524" layer="91"/>
<label x="2.54" y="78.74" size="1.778" layer="95" rot="R180"/>
<pinref part="U1" gate="G$1" pin="PB1"/>
</segment>
</net>
<net name="MPX_C" class="0">
<segment>
<pinref part="STEPS_0-7" gate="G$1" pin="C"/>
<wire x1="152.4" y1="121.92" x2="149.86" y2="121.92" width="0.1524" layer="91"/>
<label x="149.86" y="121.92" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="228.6" y1="121.92" x2="226.06" y2="121.92" width="0.1524" layer="91"/>
<label x="226.06" y="121.92" size="1.778" layer="95" rot="R180"/>
<pinref part="STEPS_8-15" gate="G$1" pin="C"/>
</segment>
<segment>
<wire x1="152.4" y1="53.34" x2="149.86" y2="53.34" width="0.1524" layer="91"/>
<label x="149.86" y="53.34" size="1.778" layer="95" rot="R180"/>
<pinref part="POTS_0-7" gate="G$1" pin="C"/>
</segment>
<segment>
<wire x1="228.6" y1="53.34" x2="226.06" y2="53.34" width="0.1524" layer="91"/>
<label x="226.06" y="53.34" size="1.778" layer="95" rot="R180"/>
<pinref part="POTS_8-15" gate="G$1" pin="C"/>
</segment>
<segment>
<wire x1="5.08" y1="76.2" x2="2.54" y2="76.2" width="0.1524" layer="91"/>
<label x="2.54" y="76.2" size="1.778" layer="95" rot="R180"/>
<pinref part="U1" gate="G$1" pin="PB2"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="D-"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="35.56" y1="48.26" x2="38.1" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="D+"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="35.56" y1="45.72" x2="38.1" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D-" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="48.26" y1="48.26" x2="50.8" y2="48.26" width="0.1524" layer="91"/>
<label x="50.8" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="D-"/>
<wire x1="114.3" y1="149.86" x2="111.76" y2="149.86" width="0.1524" layer="91"/>
<label x="109.22" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="D+" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="48.26" y1="45.72" x2="50.8" y2="45.72" width="0.1524" layer="91"/>
<label x="50.8" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="D+"/>
<wire x1="114.3" y1="152.4" x2="111.76" y2="152.4" width="0.1524" layer="91"/>
<label x="109.22" y="152.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="XTAL1" class="0">
<segment>
<pinref part="C2" gate="&gt;NAME" pin="1"/>
<wire x1="22.86" y1="147.32" x2="20.32" y2="147.32" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="2"/>
<wire x1="20.32" y1="147.32" x2="20.32" y2="144.78" width="0.1524" layer="91"/>
<wire x1="20.32" y1="147.32" x2="17.78" y2="147.32" width="0.1524" layer="91"/>
<junction x="20.32" y="147.32"/>
<label x="17.78" y="147.32" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="XTAL1"/>
<wire x1="5.08" y1="86.36" x2="2.54" y2="86.36" width="0.1524" layer="91"/>
<label x="2.54" y="86.36" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="XTAL2" class="0">
<segment>
<pinref part="C3" gate="&gt;NAME" pin="1"/>
<wire x1="22.86" y1="137.16" x2="20.32" y2="137.16" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="1"/>
<wire x1="20.32" y1="139.7" x2="20.32" y2="137.16" width="0.1524" layer="91"/>
<wire x1="20.32" y1="137.16" x2="17.78" y2="137.16" width="0.1524" layer="91"/>
<junction x="20.32" y="137.16"/>
<label x="17.78" y="137.16" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="XTAL2"/>
<wire x1="35.56" y1="86.36" x2="38.1" y2="86.36" width="0.1524" layer="91"/>
<label x="38.1" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="UVCC" class="1">
<segment>
<pinref part="U1" gate="G$1" pin="UVCC"/>
<wire x1="35.56" y1="101.6" x2="38.1" y2="101.6" width="0.1524" layer="91"/>
<label x="38.1" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="VBUS"/>
<wire x1="114.3" y1="147.32" x2="109.22" y2="147.32" width="0.1524" layer="91"/>
<pinref part="C1" gate="&gt;NAME" pin="1"/>
<wire x1="109.22" y1="147.32" x2="109.22" y2="144.78" width="0.1524" layer="91"/>
<label x="106.68" y="147.32" size="1.778" layer="95" rot="R180"/>
<wire x1="109.22" y1="147.32" x2="106.68" y2="147.32" width="0.1524" layer="91"/>
<junction x="109.22" y="147.32"/>
</segment>
</net>
<net name="VCC" class="1">
<segment>
<pinref part="J5" gate="G$1" pin="2"/>
<wire x1="60.96" y1="147.32" x2="63.5" y2="147.32" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="G$1" pin="VCC"/>
<wire x1="63.5" y1="147.32" x2="63.5" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC_2" class="1">
<segment>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="60.96" y1="144.78" x2="63.5" y2="144.78" width="0.1524" layer="91"/>
<wire x1="63.5" y1="144.78" x2="66.04" y2="144.78" width="0.1524" layer="91"/>
<wire x1="66.04" y1="144.78" x2="66.04" y2="147.32" width="0.1524" layer="91"/>
<pinref part="SUPPLY2" gate="G$1" pin="VCC_2"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="63.5" y1="144.78" x2="63.5" y2="142.24" width="0.1524" layer="91"/>
<junction x="63.5" y="144.78"/>
</segment>
<segment>
<wire x1="-17.78" y1="144.78" x2="-17.78" y2="147.32" width="0.1524" layer="91"/>
<pinref part="J12" gate="G$1" pin="3"/>
<wire x1="-20.32" y1="144.78" x2="-17.78" y2="144.78" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="147.32" x2="-30.48" y2="147.32" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="147.32" x2="-30.48" y2="134.62" width="0.1524" layer="91"/>
<pinref part="C6" gate="&gt;NAME" pin="1"/>
<wire x1="-30.48" y1="134.62" x2="-27.94" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="147.32" x2="-17.78" y2="149.86" width="0.1524" layer="91"/>
<junction x="-17.78" y="147.32"/>
<pinref part="SUPPLY3" gate="G$1" pin="VCC_2"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="STEPS_0-7" gate="G$1" pin="CH3_IN/OUT"/>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="149.86" y1="109.22" x2="152.4" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="STEPS_0-7" gate="G$1" pin="CH2_IN/OUT"/>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="149.86" y1="111.76" x2="152.4" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="STEPS_0-7" gate="G$1" pin="CH1_IN/OUT"/>
<pinref part="J1" gate="G$1" pin="3"/>
<wire x1="149.86" y1="114.3" x2="152.4" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="STEPS_0-7" gate="G$1" pin="CH0_IN/OUT"/>
<pinref part="J1" gate="G$1" pin="4"/>
<wire x1="149.86" y1="116.84" x2="152.4" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="STEPS_0-7" gate="G$1" pin="CH7_IN/OUT"/>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="149.86" y1="99.06" x2="152.4" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="STEPS_0-7" gate="G$1" pin="CH6_IN/OUT"/>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="149.86" y1="101.6" x2="152.4" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="STEPS_0-7" gate="G$1" pin="CH5_IN/OUT"/>
<pinref part="J2" gate="G$1" pin="3"/>
<wire x1="149.86" y1="104.14" x2="152.4" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="STEPS_0-7" gate="G$1" pin="CH4_IN/OUT"/>
<pinref part="J2" gate="G$1" pin="4"/>
<wire x1="149.86" y1="106.68" x2="152.4" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="STEPS_8-15" gate="G$1" pin="CH3_IN/OUT"/>
<pinref part="J6" gate="G$1" pin="1"/>
<wire x1="226.06" y1="109.22" x2="228.6" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="STEPS_8-15" gate="G$1" pin="CH2_IN/OUT"/>
<pinref part="J6" gate="G$1" pin="2"/>
<wire x1="226.06" y1="111.76" x2="228.6" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="STEPS_8-15" gate="G$1" pin="CH1_IN/OUT"/>
<pinref part="J6" gate="G$1" pin="3"/>
<wire x1="226.06" y1="114.3" x2="228.6" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="STEPS_8-15" gate="G$1" pin="CH0_IN/OUT"/>
<pinref part="J6" gate="G$1" pin="4"/>
<wire x1="226.06" y1="116.84" x2="228.6" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="STEPS_8-15" gate="G$1" pin="CH7_IN/OUT"/>
<pinref part="J7" gate="G$1" pin="1"/>
<wire x1="226.06" y1="99.06" x2="228.6" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="STEPS_8-15" gate="G$1" pin="CH6_IN/OUT"/>
<pinref part="J7" gate="G$1" pin="2"/>
<wire x1="226.06" y1="101.6" x2="228.6" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="STEPS_8-15" gate="G$1" pin="CH5_IN/OUT"/>
<pinref part="J7" gate="G$1" pin="3"/>
<wire x1="226.06" y1="104.14" x2="228.6" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="STEPS_8-15" gate="G$1" pin="CH4_IN/OUT"/>
<pinref part="J7" gate="G$1" pin="4"/>
<wire x1="226.06" y1="106.68" x2="228.6" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="POTS_8-15" gate="G$1" pin="CH3_IN/OUT"/>
<pinref part="J8" gate="G$1" pin="1"/>
<wire x1="226.06" y1="40.64" x2="228.6" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="POTS_8-15" gate="G$1" pin="CH2_IN/OUT"/>
<pinref part="J8" gate="G$1" pin="2"/>
<wire x1="226.06" y1="43.18" x2="228.6" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="POTS_8-15" gate="G$1" pin="CH1_IN/OUT"/>
<pinref part="J8" gate="G$1" pin="3"/>
<wire x1="226.06" y1="45.72" x2="228.6" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="POTS_8-15" gate="G$1" pin="CH0_IN/OUT"/>
<pinref part="J8" gate="G$1" pin="4"/>
<wire x1="226.06" y1="48.26" x2="228.6" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="POTS_8-15" gate="G$1" pin="CH7_IN/OUT"/>
<pinref part="J9" gate="G$1" pin="1"/>
<wire x1="226.06" y1="30.48" x2="228.6" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="POTS_8-15" gate="G$1" pin="CH6_IN/OUT"/>
<pinref part="J9" gate="G$1" pin="2"/>
<wire x1="226.06" y1="33.02" x2="228.6" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="POTS_8-15" gate="G$1" pin="CH5_IN/OUT"/>
<pinref part="J9" gate="G$1" pin="3"/>
<wire x1="226.06" y1="35.56" x2="228.6" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="POTS_8-15" gate="G$1" pin="CH4_IN/OUT"/>
<pinref part="J9" gate="G$1" pin="4"/>
<wire x1="226.06" y1="38.1" x2="228.6" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="POTS_0-7" gate="G$1" pin="CH3_IN/OUT"/>
<pinref part="J10" gate="G$1" pin="1"/>
<wire x1="149.86" y1="40.64" x2="152.4" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="POTS_0-7" gate="G$1" pin="CH2_IN/OUT"/>
<pinref part="J10" gate="G$1" pin="2"/>
<wire x1="149.86" y1="43.18" x2="152.4" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="POTS_0-7" gate="G$1" pin="CH1_IN/OUT"/>
<pinref part="J10" gate="G$1" pin="3"/>
<wire x1="149.86" y1="45.72" x2="152.4" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="POTS_0-7" gate="G$1" pin="CH0_IN/OUT"/>
<pinref part="J10" gate="G$1" pin="4"/>
<wire x1="149.86" y1="48.26" x2="152.4" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="POTS_0-7" gate="G$1" pin="CH7_IN/OUT"/>
<pinref part="J11" gate="G$1" pin="1"/>
<wire x1="149.86" y1="30.48" x2="152.4" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="POTS_0-7" gate="G$1" pin="CH6_IN/OUT"/>
<pinref part="J11" gate="G$1" pin="2"/>
<wire x1="149.86" y1="33.02" x2="152.4" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="POTS_0-7" gate="G$1" pin="CH5_IN/OUT"/>
<pinref part="J11" gate="G$1" pin="3"/>
<wire x1="149.86" y1="35.56" x2="152.4" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="POTS_0-7" gate="G$1" pin="CH4_IN/OUT"/>
<pinref part="J11" gate="G$1" pin="4"/>
<wire x1="149.86" y1="38.1" x2="152.4" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="J12" gate="G$1" pin="2"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="-17.78" y1="142.24" x2="-20.32" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED_DATA" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="-7.62" y1="142.24" x2="-5.08" y2="142.24" width="0.1524" layer="91"/>
<label x="-5.08" y="142.24" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PD5"/>
<wire x1="5.08" y1="45.72" x2="2.54" y2="45.72" width="0.1524" layer="91"/>
<label x="2.54" y="45.72" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="POTS_0-7" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PF0"/>
<wire x1="35.56" y1="66.04" x2="38.1" y2="66.04" width="0.1524" layer="91"/>
<label x="38.1" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="POTS_0-7" gate="G$1" pin="COM_OUT/IN"/>
<wire x1="198.12" y1="40.64" x2="200.66" y2="40.64" width="0.1524" layer="91"/>
<label x="200.66" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="POTS_8-15" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PF1"/>
<wire x1="35.56" y1="63.5" x2="38.1" y2="63.5" width="0.1524" layer="91"/>
<label x="38.1" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="POTS_8-15" gate="G$1" pin="COM_OUT/IN"/>
<wire x1="274.32" y1="40.64" x2="276.86" y2="40.64" width="0.1524" layer="91"/>
<label x="276.86" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="STEPS_0-7" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB5"/>
<wire x1="5.08" y1="68.58" x2="2.54" y2="68.58" width="0.1524" layer="91"/>
<label x="2.54" y="68.58" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="STEPS_0-7" gate="G$1" pin="COM_OUT/IN"/>
<wire x1="198.12" y1="109.22" x2="200.66" y2="109.22" width="0.1524" layer="91"/>
<label x="200.66" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="STEPS_8-15" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB6"/>
<wire x1="5.08" y1="66.04" x2="2.54" y2="66.04" width="0.1524" layer="91"/>
<label x="2.54" y="66.04" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="STEPS_8-15" gate="G$1" pin="COM_OUT/IN"/>
<wire x1="274.32" y1="109.22" x2="276.86" y2="109.22" width="0.1524" layer="91"/>
<label x="276.86" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="G_SWITCH_1" class="0">
<segment>
<pinref part="U$9" gate="G$1" pin="5"/>
<wire x1="109.22" y1="93.98" x2="101.6" y2="93.98" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="101.6" y1="93.98" x2="101.6" y2="96.52" width="0.1524" layer="91"/>
<wire x1="101.6" y1="93.98" x2="99.06" y2="93.98" width="0.1524" layer="91"/>
<junction x="101.6" y="93.98"/>
<label x="96.266" y="91.694" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PD6"/>
<wire x1="5.08" y1="43.18" x2="2.54" y2="43.18" width="0.1524" layer="91"/>
<label x="2.54" y="43.18" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="G_SWITCH_2" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="5"/>
<wire x1="109.22" y1="58.42" x2="101.6" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="101.6" y1="58.42" x2="101.6" y2="60.96" width="0.1524" layer="91"/>
<wire x1="101.6" y1="58.42" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
<junction x="101.6" y="58.42"/>
<label x="96.266" y="56.134" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PD7"/>
<wire x1="5.08" y1="40.64" x2="2.54" y2="40.64" width="0.1524" layer="91"/>
<label x="2.54" y="40.64" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="G_POT_2" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="2"/>
<wire x1="109.22" y1="71.12" x2="106.68" y2="71.12" width="0.1524" layer="91"/>
<label x="106.68" y="71.12" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PF5"/>
<wire x1="35.56" y1="58.42" x2="38.1" y2="58.42" width="0.1524" layer="91"/>
<label x="38.1" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="G_POT_1" class="0">
<segment>
<pinref part="U$9" gate="G$1" pin="2"/>
<wire x1="109.22" y1="106.68" x2="106.68" y2="106.68" width="0.1524" layer="91"/>
<label x="106.68" y="106.68" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PF4"/>
<wire x1="35.56" y1="60.96" x2="38.1" y2="60.96" width="0.1524" layer="91"/>
<label x="38.1" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
