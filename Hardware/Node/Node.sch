<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="16" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="AG">
<packages>
<package name="PRS11R-4XXF-SXXXXX">
<wire x1="-0.3" y1="0.7" x2="-0.3" y2="13.8" width="0.5" layer="51"/>
<wire x1="-0.3" y1="13.8" x2="11.4" y2="13.8" width="0.5" layer="51"/>
<wire x1="11.4" y1="13.8" x2="11.4" y2="0.7" width="0.5" layer="51"/>
<wire x1="11.4" y1="0.7" x2="-0.3" y2="0.7" width="0.5" layer="51"/>
<pad name="2" x="5.55" y="0.7" drill="1.1" diameter="1.5" shape="square"/>
<pad name="1" x="3.05" y="0.7" drill="1.1" diameter="1.5" shape="square"/>
<pad name="3" x="8.05" y="0.7" drill="1.1" diameter="1.5" shape="square"/>
<pad name="4" x="3.05" y="14.5" drill="1.1" diameter="1.5" shape="square"/>
<pad name="5" x="8.05" y="14.5" drill="1.1" diameter="1.5" shape="square"/>
<wire x1="-0.9" y1="9.5" x2="0.9" y2="9.5" width="0.127" layer="20"/>
<wire x1="0.9" y1="9.5" x2="0.9" y2="6.9" width="0.127" layer="20"/>
<wire x1="0.9" y1="6.9" x2="-0.9" y2="6.9" width="0.127" layer="20"/>
<wire x1="-0.9" y1="6.9" x2="-0.9" y2="9.5" width="0.127" layer="20"/>
<wire x1="10.2" y1="6.9" x2="10.2" y2="9.5" width="0.127" layer="20"/>
<wire x1="10.2" y1="9.5" x2="12" y2="9.5" width="0.127" layer="20"/>
<wire x1="12" y1="6.9" x2="10.2" y2="6.9" width="0.127" layer="20"/>
<wire x1="12" y1="9.5" x2="12" y2="6.9" width="0.127" layer="20"/>
<rectangle x1="0" y1="0" x2="11.1" y2="14.5" layer="29"/>
<text x="1.7" y="7.5" size="1.778" layer="25">&gt;name</text>
<rectangle x1="-1.27" y1="6.35" x2="1.27" y2="10.16" layer="1"/>
<rectangle x1="9.851896875" y1="6.35" x2="12.391896875" y2="10.16" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="PRS11R-SWITCH">
<wire x1="0" y1="0" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="15.24" y2="20.32" width="0.254" layer="94"/>
<wire x1="15.24" y1="20.32" x2="0" y2="20.32" width="0.254" layer="94"/>
<wire x1="0" y1="20.32" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="17.78" length="short"/>
<pin name="2" x="-2.54" y="15.24" length="short" direction="out"/>
<pin name="3" x="-2.54" y="12.7" length="short"/>
<pin name="5" x="-2.54" y="2.54" length="short"/>
<pin name="4" x="-2.54" y="5.08" length="short"/>
<text x="0.254" y="20.828" size="1.778" layer="95">G$1</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PRS11R-4XXF-SXXXXX">
<gates>
<gate name="G$1" symbol="PRS11R-SWITCH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PRS11R-4XXF-SXXXXX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-LED">
<description>&lt;h3&gt;SparkFun LEDs&lt;/h3&gt;
This library contains discrete LEDs for illumination or indication, but no displays.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="WS2812B">
<description>&lt;h3&gt;WS2812B&lt;/h3&gt;

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Size: 5.0 x 5.0 mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt; &lt;a href="http://cdn.sparkfun.com/datasheets/BreakoutBoards/WS2812B.pdf"&gt;WS2812B&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device:
&lt;ul&gt;&lt;li&gt;WS2812B&lt;/li&gt;</description>
<smd name="1" x="2.577" y="-1.6" dx="1.651" dy="1" layer="1" rot="R180"/>
<smd name="4" x="-2.577" y="-1.6" dx="1.651" dy="1" layer="1" rot="R180"/>
<smd name="3" x="-2.577" y="1.6" dx="1.651" dy="1" layer="1" rot="R180"/>
<smd name="2" x="2.577" y="1.6" dx="1.651" dy="1" layer="1" rot="R180"/>
<wire x1="2.5" y1="-2.5" x2="2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="2.5" x2="-2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="2.5" x2="-2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.127" layer="51"/>
<text x="0" y="2.794" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="2.4892" y1="-2.362203125" x2="2.4892" y2="-2.616203125" width="0.2032" layer="21"/>
<wire x1="2.4892" y1="-2.616203125" x2="2.4384" y2="-2.616203125" width="0.2032" layer="21"/>
<wire x1="2.4384" y1="-2.616203125" x2="2.4384" y2="-2.611121875" width="0.2032" layer="21"/>
<wire x1="2.4384" y1="-2.611121875" x2="-2.512059375" y2="-2.611121875" width="0.2032" layer="21"/>
<wire x1="-2.512059375" y1="-2.611121875" x2="-2.512059375" y2="-2.37998125" width="0.2032" layer="21"/>
<wire x1="2.50951875" y1="2.319021875" x2="2.50951875" y2="2.5908" width="0.2032" layer="21"/>
<wire x1="2.50951875" y1="2.5908" x2="2.45871875" y2="2.5908" width="0.2032" layer="21"/>
<wire x1="2.45871875" y1="2.5908" x2="2.45871875" y2="2.593340625" width="0.2032" layer="21"/>
<wire x1="2.45871875" y1="2.593340625" x2="-2.50951875" y2="2.593340625" width="0.2032" layer="21"/>
<wire x1="-2.50951875" y1="2.593340625" x2="-2.50951875" y2="2.36728125" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.889" x2="2.54" y2="-0.8128" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.8636" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="0" y2="2.54" width="0.2032" layer="21"/>
<circle x="-2.8575" y="2.8575" radius="0.2032" width="0" layer="21"/>
<circle x="-1.27" y="1.905" radius="0.2032" width="0" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="LED-TRICOLOR-WS28X1">
<description>&lt;h3&gt;WS28X1 RGB LED - I2C Control&lt;/h3&gt;
&lt;p&gt;4 pin RGB LED with I2C Controller built-in&lt;/p&gt;</description>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-12.7" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-5.08" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="-12.7" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.27" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.27" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="-0.508" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.032" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-1.778" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.778" x2="-0.508" y2="-3.048" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-3.048" x2="-0.508" y2="-4.318" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-4.318" x2="0.762" y2="-3.048" width="0.254" layer="94"/>
<wire x1="0.762" y1="-3.048" x2="-0.508" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0.762" y1="-1.778" x2="0.762" y2="-3.048" width="0.254" layer="94"/>
<wire x1="0.762" y1="-3.048" x2="0.762" y2="-4.318" width="0.254" layer="94"/>
<wire x1="0.762" y1="-3.048" x2="2.032" y2="-3.048" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-3.048" x2="-1.778" y2="-3.048" width="0.254" layer="94"/>
<wire x1="-0.508" y1="4.318" x2="-0.508" y2="3.048" width="0.254" layer="94"/>
<wire x1="-0.508" y1="3.048" x2="-0.508" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="0.762" y2="3.048" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.048" x2="-0.508" y2="4.318" width="0.254" layer="94"/>
<wire x1="0.762" y1="4.318" x2="0.762" y2="3.048" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.048" x2="0.762" y2="1.778" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.048" x2="2.032" y2="3.048" width="0.254" layer="94"/>
<wire x1="-0.508" y1="3.048" x2="-1.778" y2="3.048" width="0.254" layer="94"/>
<pin name="VDD" x="-15.24" y="5.08" visible="pin" length="short"/>
<pin name="VSS" x="12.7" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="DI" x="-15.24" y="-2.54" visible="pin" length="short"/>
<pin name="DO" x="12.7" y="-2.54" visible="pin" length="short" rot="R180"/>
<text x="-2.54" y="8.382" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="-2.54" y="-7.62" size="1.778" layer="95" font="vector" align="bottom-center">&gt;VALUE</text>
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="-5.08" width="0.254" layer="94" style="shortdash"/>
<wire x1="2.54" y1="7.62" x2="2.54" y2="-5.08" width="0.254" layer="94" style="shortdash"/>
<text x="-1.524" y="5.5118" size="1.27" layer="94">RGB</text>
<text x="-3.175" y="-2.159" size="1.27" layer="94" rot="R90">WS28x1</text>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.254" layer="94" style="shortdash"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="WS2812B" prefix="D" uservalue="yes">
<description>&lt;h3&gt;WS2812B SMD addressable RGB LED&lt;/h3&gt;
5x5mm SMD LED with built-in controller IC.

&lt;p&gt;&lt;li&gt;&lt;b&gt;Color:&lt;/b&gt; Red/Green/Blue&lt;/li&gt;
&lt;li&gt;&lt;b&gt;mC Rating: &lt;/b&gt;RGB: 390, 660, 180 mcd&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Forward Voltage: &lt;/b&gt; RGB: 2.0, 3.0, 3.0 V&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Packages:&lt;/b&gt;WS2812B&lt;/li&gt;
&lt;/p&gt;
&lt;p&gt;SparkFun Products:
&lt;ul&gt;&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13794”&gt;Blynk Board - ESP8266&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="LED-TRICOLOR-WS28X1" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="WS2812B">
<connects>
<connect gate="G$1" pin="DI" pad="4"/>
<connect gate="G$1" pin="DO" pad="2"/>
<connect gate="G$1" pin="VDD" pad="1"/>
<connect gate="G$1" pin="VSS" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-12503"/>
<attribute name="VALUE" value="WS2812B" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="00-DDD-fab" urn="urn:adsk.eagle:library:14307292">
<packages>
<package name="R1206" urn="urn:adsk.eagle:footprint:14307382/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W" urn="urn:adsk.eagle:footprint:14307383/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206FAB" urn="urn:adsk.eagle:footprint:14307386/1" library_version="1">
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="R1206" urn="urn:adsk.eagle:package:14307471/1" type="box" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="R1206"/>
</packageinstances>
</package3d>
<package3d name="R1206W" urn="urn:adsk.eagle:package:14307470/1" type="box" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<packageinstances>
<packageinstance name="R1206W"/>
</packageinstances>
</package3d>
<package3d name="R1206FAB" urn="urn:adsk.eagle:package:14307467/1" type="box" library_version="1">
<packageinstances>
<packageinstance name="R1206FAB"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="R-US" urn="urn:adsk.eagle:symbol:14307302/1" library_version="1">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RES-US" urn="urn:adsk.eagle:component:14307620/1" prefix="R" uservalue="yes" library_version="1">
<description>&lt;b&gt;Resistor (US Symbol)&lt;/b&gt;
&lt;p&gt;
Variants with postfix FAB are widened to allow the routing of internal traces</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14307471/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14307470/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206FAB" package="R1206FAB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14307467/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="00-DDD-CONNECTORS" urn="urn:adsk.eagle:library:14328941">
<packages>
<package name="MOLEX-KK254-2X-TOP" urn="urn:adsk.eagle:footprint:14328957/1" library_version="14">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<wire x1="-2.55" y1="-2.9" x2="-2.55" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.55" y1="1.27" x2="-2.55" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.55" y1="-2.9" x2="2.55" y2="-2.9" width="0.1524" layer="21"/>
<wire x1="-2.55" y1="1.27" x2="-2.55" y2="2.9" width="0.1524" layer="21"/>
<wire x1="2.55" y1="2.9" x2="-2.55" y2="2.9" width="0.1524" layer="21"/>
<wire x1="2.55" y1="1.27" x2="2.55" y2="-2.9" width="0.1524" layer="21"/>
<wire x1="2.55" y1="2.9" x2="2.55" y2="1.27" width="0.1524" layer="21"/>
<pad name="2" x="-1.27" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="1" x="1.27" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R90"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.27" layer="27">2X1</text>
</package>
<package name="MOLEX-KK254-2X-SIDE" urn="urn:adsk.eagle:footprint:14328974/1" library_version="14">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<wire x1="-2.55" y1="-5.9" x2="-2.55" y2="-2.72" width="0.1524" layer="21"/>
<wire x1="2.55" y1="-2.72" x2="-2.55" y2="-2.72" width="0.1524" layer="21"/>
<wire x1="-2.55" y1="-5.9" x2="-1.27" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-5.9" x2="1.27" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-5.9" x2="2.55" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="2.55" y1="-2.72" x2="2.55" y2="-5.9" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-2.54" width="0.6096" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-2.54" width="0.6096" layer="21"/>
<wire x1="-1.27" y1="-5.9" x2="-1.27" y2="-14.42" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-14.42" x2="1.27" y2="-14.42" width="0.1524" layer="51"/>
<wire x1="1.27" y1="-14.42" x2="1.27" y2="-5.9" width="0.1524" layer="51"/>
<pad name="2" x="-1.27" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="1" x="1.27" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R90"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-5.08" size="1.27" layer="27">2X1</text>
</package>
<package name="MOLEX-KK254-4X-TOP" urn="urn:adsk.eagle:footprint:14328975/2" library_version="14">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<wire x1="-5.1" y1="-2.9" x2="-5.1" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.1" y1="1.27" x2="-5.1" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.1" y1="1.27" x2="5.1" y2="-2.9" width="0.1524" layer="21"/>
<wire x1="-5.1" y1="-2.9" x2="5.1" y2="-2.9" width="0.1524" layer="21"/>
<wire x1="-5.1" y1="1.27" x2="-5.1" y2="2.9" width="0.1524" layer="21"/>
<wire x1="5.1" y1="2.9" x2="-5.1" y2="2.9" width="0.1524" layer="21"/>
<wire x1="5.1" y1="2.9" x2="5.1" y2="1.27" width="0.1524" layer="21"/>
<pad name="4" x="-3.81" y="0" drill="0.9" diameter="1.4" shape="long" rot="R270"/>
<pad name="3" x="-1.27" y="0" drill="0.9" diameter="1.4" shape="long" rot="R270"/>
<pad name="2" x="1.27" y="0" drill="0.9" diameter="1.4" shape="long" rot="R270"/>
<pad name="1" x="3.81" y="0" drill="0.9" diameter="1.4" shape="long" rot="R270"/>
<text x="-5.08" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="27">4X1</text>
</package>
<package name="MOLEX-KK254-4X-SIDE" urn="urn:adsk.eagle:footprint:14328978/1" library_version="14">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<pad name="4" x="-3.81" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R270"/>
<pad name="3" x="-1.27" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R270"/>
<pad name="2" x="1.27" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R270"/>
<pad name="1" x="3.81" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R270"/>
<text x="-5.08" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-5.08" size="1.27" layer="27">4X1</text>
<wire x1="-5.09" y1="-5.9" x2="-5.09" y2="-2.72" width="0.1524" layer="21"/>
<wire x1="5.09" y1="-2.72" x2="-5.09" y2="-2.72" width="0.1524" layer="21"/>
<wire x1="-5.09" y1="-5.9" x2="-3.81" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-5.9" x2="3.81" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-5.9" x2="5.09" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="5.09" y1="-2.72" x2="5.09" y2="-5.9" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="-2.54" width="0.6096" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="3.81" y2="-2.54" width="0.6096" layer="21"/>
<wire x1="-3.81" y1="-5.9" x2="-3.81" y2="-15.69" width="0.1524" layer="51"/>
<wire x1="-3.81" y1="-15.69" x2="3.81" y2="-15.69" width="0.1524" layer="51"/>
<wire x1="3.81" y1="-15.69" x2="3.81" y2="-5.9" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-2.54" width="0.6096" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-2.54" width="0.6096" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="10X02MTA" urn="urn:adsk.eagle:package:14328993/1" type="box" library_version="14">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<packageinstances>
<packageinstance name="MOLEX-KK254-2X-TOP"/>
</packageinstances>
</package3d>
<package3d name="MOLEX-KK254-2X-SIDE" urn="urn:adsk.eagle:package:14329002/1" type="model" library_version="14">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<packageinstances>
<packageinstance name="MOLEX-KK254-2X-SIDE"/>
</packageinstances>
</package3d>
<package3d name="10X04MTA" urn="urn:adsk.eagle:package:14328981/2" type="box" library_version="14">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<packageinstances>
<packageinstance name="MOLEX-KK254-4X-TOP"/>
</packageinstances>
</package3d>
<package3d name="MOLEX-KK254-4X-SIDE" urn="urn:adsk.eagle:package:14328995/1" type="model" library_version="14">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<packageinstances>
<packageinstance name="MOLEX-KK254-4X-SIDE"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MTA-1_2" urn="urn:adsk.eagle:symbol:14328949/1" library_version="14">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<text x="5.08" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="-1.27" size="1.27" layer="95">1</text>
<pin name="1" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="2" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="MTA-1_4" urn="urn:adsk.eagle:symbol:14328942/1" library_version="14">
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="6.35" y1="-1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="6.35" y1="-1.905" x2="6.35" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.27" x2="6.35" y2="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="5.08" y="0" radius="0.635" width="0.254" layer="94"/>
<text x="7.62" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="7.62" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="-1.27" size="1.27" layer="95">1</text>
<pin name="1" x="-2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="3" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="4" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOLEX-KK254-2X" urn="urn:adsk.eagle:component:14329008/1" prefix="J" uservalue="yes" library_version="14">
<description>&lt;b&gt;AMP connector&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MTA-1_2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MOLEX-KK254-2X-TOP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14328993/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDE" package="MOLEX-KK254-2X-SIDE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14329002/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOLEX-KK254-4X" urn="urn:adsk.eagle:component:14329006/2" prefix="J" uservalue="yes" library_version="14">
<description>&lt;b&gt;AMP connector&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MTA-1_4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MOLEX-KK254-4X-TOP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14328981/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDE" package="MOLEX-KK254-4X-SIDE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14328995/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.3556" drill="0">
<clearance class="0" value="0.5"/>
</class>
<class number="1" name="wider" width="0.508" drill="0">
<clearance class="1" value="0.5"/>
</class>
</classes>
<parts>
<part name="D1" library="SparkFun-LED" deviceset="WS2812B" device="" value="WS2812B"/>
<part name="R1" library="00-DDD-fab" library_urn="urn:adsk.eagle:library:14307292" deviceset="RES-US" device="1206FAB" package3d_urn="urn:adsk.eagle:package:14307467/1" value="10K"/>
<part name="PRS11R" library="AG" deviceset="PRS11R-4XXF-SXXXXX" device=""/>
<part name="J1" library="00-DDD-CONNECTORS" library_urn="urn:adsk.eagle:library:14328941" deviceset="MOLEX-KK254-2X" device="" package3d_urn="urn:adsk.eagle:package:14328993/1"/>
<part name="J2" library="00-DDD-CONNECTORS" library_urn="urn:adsk.eagle:library:14328941" deviceset="MOLEX-KK254-4X" device="" package3d_urn="urn:adsk.eagle:package:14328981/2"/>
<part name="J3" library="00-DDD-CONNECTORS" library_urn="urn:adsk.eagle:library:14328941" deviceset="MOLEX-KK254-4X" device="" package3d_urn="urn:adsk.eagle:package:14328981/2"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="D1" gate="G$1" x="63.5" y="40.64" smashed="yes">
<attribute name="NAME" x="60.96" y="49.022" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="60.96" y="33.02" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R1" gate="G$1" x="119.38" y="81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="123.19" y="79.7814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="123.19" y="84.582" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PRS11R" gate="G$1" x="66.04" y="78.74" smashed="yes" rot="R180"/>
<instance part="J1" gate="G$1" x="121.92" y="71.12" smashed="yes" rot="R270">
<attribute name="NAME" x="121.92" y="66.04" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="118.11" y="66.04" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J2" gate="G$1" x="121.92" y="58.42" smashed="yes" rot="R270">
<attribute name="NAME" x="121.92" y="50.8" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="118.11" y="50.8" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J3" gate="G$1" x="121.92" y="43.18" smashed="yes" rot="R270">
<attribute name="NAME" x="121.92" y="35.56" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="118.11" y="35.56" size="1.778" layer="96" rot="R270"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="POT_V" class="1">
<segment>
<wire x1="68.58" y1="66.04" x2="71.12" y2="66.04" width="0.1524" layer="91"/>
<label x="71.12" y="66.04" size="1.778" layer="95"/>
<pinref part="PRS11R" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="119.38" y1="60.96" x2="116.84" y2="60.96" width="0.1524" layer="91"/>
<label x="119.38" y="63.5" size="1.778" layer="95" rot="R180"/>
<pinref part="J2" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="119.38" y1="45.72" x2="116.84" y2="45.72" width="0.1524" layer="91"/>
<label x="119.38" y="48.26" size="1.778" layer="95" rot="R180"/>
<pinref part="J3" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="114.3" y1="81.28" x2="111.76" y2="81.28" width="0.1524" layer="91"/>
<label x="111.76" y="81.28" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="POT_SIGNAL" class="0">
<segment>
<wire x1="68.58" y1="63.5" x2="71.12" y2="63.5" width="0.1524" layer="91"/>
<label x="71.12" y="63.5" size="1.778" layer="95"/>
<pinref part="PRS11R" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="119.38" y1="71.12" x2="116.84" y2="71.12" width="0.1524" layer="91"/>
<label x="119.38" y="73.66" size="1.778" layer="95" rot="R180"/>
<pinref part="J1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="NEO_V" class="1">
<segment>
<pinref part="D1" gate="G$1" pin="VDD"/>
<wire x1="48.26" y1="45.72" x2="45.72" y2="45.72" width="0.1524" layer="91"/>
<label x="45.72" y="48.26" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="119.38" y1="58.42" x2="116.84" y2="58.42" width="0.1524" layer="91"/>
<label x="119.38" y="60.96" size="1.778" layer="95" rot="R180"/>
<pinref part="J2" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="119.38" y1="43.18" x2="116.84" y2="43.18" width="0.1524" layer="91"/>
<label x="119.38" y="45.72" size="1.778" layer="95" rot="R180"/>
<pinref part="J3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="GND" class="1">
<segment>
<wire x1="76.2" y1="45.72" x2="78.74" y2="45.72" width="0.1524" layer="91"/>
<label x="78.74" y="45.72" size="1.778" layer="95"/>
<pinref part="D1" gate="G$1" pin="VSS"/>
</segment>
<segment>
<wire x1="68.58" y1="60.96" x2="71.12" y2="60.96" width="0.1524" layer="91"/>
<label x="71.12" y="60.96" size="1.778" layer="95"/>
<pinref part="PRS11R" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="119.38" y1="53.34" x2="116.84" y2="53.34" width="0.1524" layer="91"/>
<label x="119.38" y="55.88" size="1.778" layer="95" rot="R180"/>
<pinref part="J2" gate="G$1" pin="4"/>
</segment>
<segment>
<wire x1="119.38" y1="38.1" x2="116.84" y2="38.1" width="0.1524" layer="91"/>
<label x="119.38" y="40.64" size="1.778" layer="95" rot="R180"/>
<pinref part="J3" gate="G$1" pin="4"/>
</segment>
<segment>
<pinref part="PRS11R" gate="G$1" pin="4"/>
<wire x1="68.58" y1="73.66" x2="71.12" y2="73.66" width="0.1524" layer="91"/>
<label x="71.12" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="DATA_OUT" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="DO"/>
<wire x1="76.2" y1="38.1" x2="78.74" y2="38.1" width="0.1524" layer="91"/>
<label x="78.74" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="119.38" y1="55.88" x2="116.84" y2="55.88" width="0.1524" layer="91"/>
<label x="119.38" y="58.42" size="1.778" layer="95" rot="R180"/>
<pinref part="J2" gate="G$1" pin="3"/>
</segment>
</net>
<net name="DATA_IN" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="DI"/>
<wire x1="48.26" y1="38.1" x2="45.72" y2="38.1" width="0.1524" layer="91"/>
<label x="45.72" y="40.64" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="119.38" y1="40.64" x2="116.84" y2="40.64" width="0.1524" layer="91"/>
<label x="119.38" y="43.18" size="1.778" layer="95" rot="R180"/>
<pinref part="J3" gate="G$1" pin="3"/>
</segment>
</net>
<net name="SWITCH" class="0">
<segment>
<wire x1="119.38" y1="68.58" x2="116.84" y2="68.58" width="0.1524" layer="91"/>
<label x="119.38" y="71.12" size="1.778" layer="95" rot="R180"/>
<pinref part="J1" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="127" y1="81.28" x2="124.46" y2="81.28" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<label x="127" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PRS11R" gate="G$1" pin="5"/>
<wire x1="68.58" y1="76.2" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
<label x="71.12" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
