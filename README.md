# SicSec


## TL;DR

I'm trying to build a very fidgety sequencer, but it's taking me some time, ok!?!

## Concept

I want to craft from scratch (concept, product, electronics & software) a digital MIDI sequencer that can be operated with rotary potentiometers, giving the user an analog feeling.

It will contain 16 steps that - according to another potentiometer as a selector - can control:

- Pitch
- Velocity
- Duration

A momentary switch on the potentiometer will enable or disable the step, and a related RGB leds will give visual cues about values and operations.

## To Do

- Everything!!!

## Roadmap

![Roadmap](roadmap.svg)
