#include <Adafruit_NeoPixel.h>

#define SERIAL_LOG

#define stepsAmount 16

uint8_t pots[stepsAmount] = {
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  10,
  11,
  12,
  13,
  14,
  15,
  16
};

enum parameter { pitch, velocity, duration, end_of_parameters };
parameter currentParameter;

struct StepObject {
  uint8_t parameters[end_of_parameters];
  uint8_t pitch;
  uint8_t velocity;
  uint8_t duration;
  uint8_t pot;
  uint8_t button;
};

StepObject steps[stepsAmount];

uint8_t potsBuffer[stepsAmount];

#define pixelData 17
#define pixelsAmount stepsAmount + 3 // including controllers beyond steps
Adafruit_NeoPixel leds(pixelsAmount, pixelData, NEO_RGB + NEO_KHZ800);
const uint32_t white = leds.Color(255, 255, 255);
const uint32_t black = leds.Color(0, 0, 0);

void setup() {
  #ifdef SERIAL_LOG
  Serial.begin(115200);
  #endif

  currentParameter = parameters::PITCH;
}

void loop() {
  readInput();
  updateSystem();
  renderUpdates();
  sendInstructions();  
}

void sendSerial(String m) {
  #ifdef SERIAL_LOG
  Serial.println(m);
  #endif
}

void readInput() {
  // reading potentiometers
  for (short i = 0; i < stepsAmount; i++) {
    potsBuffer[i] = map(analogRead(pots[i]), 0, 1023, 0, 255);    
  }
}

void updateSystem() {
  for (short i = 0; i < stepsAmount; i++) {
    steps[i][currentParameter] = map(analogRead(steps[i].pot), 0, 1023, 0, 255);    
  }
}

void renderUpdates() {
  //
}

void sendInstructions() {
  //
}
